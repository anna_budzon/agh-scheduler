const mongoose = require('mongoose');

const TYPES = [
  'FIRST',
  'SECOND'
];

const MODES = [
  'PART_TIME',
  'FULL_TIME'
]

const majorGroupSchema = mongoose.Schema({
  parentMajor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Major',
    required: true
  },
  type: {
    type: String,
    enum: TYPES,
    required: true
  },
  mode: {
    type: String,
    enum: MODES,
    required: true
  },
  year: {
    type: Number
  },
  semester: {
    type: Number
  },
  beginYear: {
    type: Number
  },
  studentGroups: {
    type: {
      labGroups: { type: Number },
      regularGroups: { type: Number }
    }
  },
  students: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Student",
    required: true
  }],
  schedule: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Schedule'
  }
});

module.exports = mongoose.model('MajorGroup', majorGroupSchema);
