const mongoose = require('mongoose');

const DayType = [
  'MONDAY',
  'TUESDAY',
  'WEDNESDAY',
  'THURSDAY',
  'FRIDAY',
  'SATURDAY',
  'SUNDAY'
];

const scheduleSchema = mongoose.Schema({
  completed: { type: Boolean },
  days: {
    type: [{
      dayType: {
        type: String,
        enum: DayType
      },
      classes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Class'
      }]
    }],
    required: true
  },
  major: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Major'
  },
  majorGroup: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MajorGroup'
  },
  departmentId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Department'
  },
  teacherId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  roomId: {
    type: mongoose.Schema.Types.ObjectId,
  }
});

module.exports = mongoose.model('Schedule', scheduleSchema);
