const mongoose = require('mongoose');

const ROLES = [
  'ADMIN',
  'TEACHER',
  'STUDENT'
];

const roleSchema = mongoose.Schema({
    role: { type: String, enum: ROLES, required: true },
    department: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Department",
      required: true
    }
});

module.exports = mongoose.model('Role', roleSchema);
