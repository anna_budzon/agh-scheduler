const mongoose = require('mongoose');

const TYPES = [
  'FIRST',
  'SECOND'
];

const MODES = [
  'PART_TIME',
  'FULL_TIME'
];

const majorSchema = mongoose.Schema({
    departmentId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    majorType: [{
      type: String,
      enum: TYPES
    }],
    majorMode: [{
      type: String,
      enum: MODES
    }],
    groups: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "MajorGroup"
    }]
});

exports.MODES = MODES;
exports.TYPES = TYPES;
module.exports = mongoose.model('Major', majorSchema);
