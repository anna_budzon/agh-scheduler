const mongoose = require('mongoose');

const courseSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  majorType: {
    type: String,
    enum: ['FIRST', 'SECOND']
  },
  majorMode: {
    type: String,
    enum: ['FULL_TIME', 'PART_TIME']
  },
  major: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Major'
  },
  semester: {
    type: String,
    required: true
  },
  hours: {
    type: {
      lecture: Number,
      lab: Number,
      itLab: Number,
      recitation: Number,
      project: Number,
      different: Number
    },
    required: true
  },
  departmentId: {
    type: mongoose.Schema.Types.ObjectId
  }
});

module.exports = mongoose.model('Course', courseSchema);
