const mongoose = require('mongoose');

const ROOM_TYPE = [
  'PE',
  'OFFICE',
  'LAB',
  'IT',
  'REGULAR',
  'LECTURE',
  'SPECIAL'
]

const roomSchema = mongoose.Schema({
  number: {
    type: String,
    required: true
  },
  building: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Building",
    required: true
  },
  limitOfPlaces: {
    type: Number
  },
  type: {
    type: String,
    enum: ROOM_TYPE,
    required: true
  },
  departmentId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Department"
  },
  schedule: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Schedule"
  }
});

module.exports = mongoose.model('Room', roomSchema);
