const mongoose = require('mongoose');

const teacherSchema = mongoose.Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    title: { type: String, required: true },
    office: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Room" },
    departmentId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Department"
    },
    schedule: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Schedule'
    }
});

module.exports = mongoose.model('Teacher', teacherSchema);
