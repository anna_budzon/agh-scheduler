const mongoose = require('mongoose');

const DAY = [
  1, 2, 3, 4, 5, 6, 7
];

const classTimeSchema = mongoose.Schema({
    id: { type: Number },
    day: { type: String, enum: DAY, required: true},
    from: { type: String, required: true },
    to: { type: String, required: true },
    // length: { type: Number, required: true }, - future
});

module.exports = mongoose.model('ClassTime', classTimeSchema);
