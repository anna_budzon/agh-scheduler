const mongoose = require('mongoose');

const buildingSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  rooms: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room'
  }],
});

module.exports = mongoose.model('Building', buildingSchema);
