const mongoose = require('mongoose');

const studentSchema = mongoose.Schema({
  index: {
    type: String,
    required: true
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  pesel: {
    type: String,
    required: true
  },
  groupMajorId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  major: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Major'
  },
  departmentId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  classGroup: {
    type: {
      labGroup: { type: Number },
      regularGroup: { type: Number }
    }
  }
});

module.exports = mongoose.model('Student', studentSchema);
