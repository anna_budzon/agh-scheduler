const mongoose = require('mongoose');

const departmentSchema = mongoose.Schema({
    name: { type: String, required: true },
    majors: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Major"
    }],
});

module.exports = mongoose.model('Department', departmentSchema);
