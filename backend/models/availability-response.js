const mongoose = require('mongoose');

const OccupationStateEnum = [
  OCCUPIED = 1,
  AVAILABLE = 2,
  NONE = 3
];

const ConflictTypeEnum = [
  TEACHER_BUSY = 1,
  CLASS_OCCUPIED = 2,
  GROUP_BUSY = 3,
  UNKNOWN = 4
]

const timeSchema = mongoose.Schema({
  id: {
    type: String
  },
  state: {
    type: String,
    enum: OccupationStateEnum
  },
  conflicts: {
    type: [{
      message: {
        type: String
      },
      conflictType: {
        type: String,
        enum: ConflictTypeEnum
      }
    }]
  }
});

const availabilityResponseSchema = mongoose.Schema({
  schedule: {
    type: {
      MONDAY: {
        type: [ timeSchema ]
      },
      TUESDAY: {
        type: [ timeSchema ]
      },
      WEDNESDAY: {
        type: [ timeSchema ]
      },
      THURSDAY: {
        type: [ timeSchema ]
      },
      FRIDAY:{
        type: [ timeSchema ]
      },
      SATURDAY: {
        type: [ timeSchema ]
      },
      SUNDAY: {
        type: [ timeSchema ]
      }
    }
  }
});

module.exports = mongoose.model('AvailabilityResponse', availabilityResponseSchema);
