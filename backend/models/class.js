const mongoose = require('mongoose');

const CLASS_TYPE = [
  'LAB',
  'IT',
  'LECTURE',
  'RECITATION',
  'PROJECT',
  'SPECIAL'
];

const REGULARITY = [
  'EVERY_WEEK',
  'FORTNIGHTLY'
];

const DAY_TYPE = [
  'MONDAY',
  'TUESDAY',
  'WEDNESDAY',
  'THURSDAY',
  'FRIDAY',
  'SATURDAY',
  'SUNDAY'
];

const classSchema = mongoose.Schema({
  name: { type: String  },
  course: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course'
  },
  time: {
    type: {
      id: { type: Number },
      day: { type: String, enum: DAY_TYPE, required: true},
      from: { type: String, required: true },
      to: { type: String, required: true },
    }
  },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room'
  },
  teacher: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Teacher"
  },
  scheduleId: {
    type: mongoose.Schema.Types.ObjectId
  },
  regularity: {
    type: String,
    enum: REGULARITY
  },
  type: {
    type: String,
    enum: CLASS_TYPE
  },
  studentGroupNr: {
    type: {
      labGroup: {
        type: Number
      },
      regularGroup: {
        type: Number
      }
    }
  }
});

module.exports = mongoose.model('Class', classSchema);
