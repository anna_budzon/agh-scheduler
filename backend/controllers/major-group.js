const MajorGroup = require('../models/major-group');
const Major = require('../models/major');
const Student = require('../models/student');

// Creates MajorGroup
exports.createMajorGroup = (req, res) => {
  const majorGroup = new MajorGroup({
    parentMajor: req.body.parentMajor,
    type: req.body.type,
    mode: req.body.mode,
    year: req.body.year,
    semester: req.body.semester,
    beginYear: req.body.beginYear,
    students: req.body.students
  });

  if (majorGroup.students.length) {
    majorGroup.studentGroups.labGroups = Math.ceil(majorGroup.students.length / 15);
    majorGroup.studentGroups.regularGroups = Math.ceil(majorGroup.students.length / 30);


    majorGroup.students.sort((a, b) => {
      if (a.lastName < b.lastName)
        return -1;
      if (a.lastName > b.lastName)
        return 1;
      return 0;
    });

    let grLab = 1;
    let gr = 1;
    majorGroup.students.forEach((st, i) => {
      if (i !== 0 && i % 15 === 0) grLab++;
      if (i !== 0 && i % 30 === 0) gr++;

      Student.updateOne({
          _id: st.id
        }, {
          $set: {
            "classGroup": {
              labGroup: grLab,
              regularGroup: gr
            }
          }
        }).then((result) => {
          console.log(result);
        })
        .catch(error => {
          res.status(500).json({
            message: "Updating student failed!",
            error: error
          });
        });
    });
  }

  majorGroup.save()
    .then((createdMajorGroup) => {
      Major.updateOne({
          _id: req.body.parentMajor
        }, {
          $push: {
            groups: createdMajorGroup.id
          }
        }).then((result) => {
          res.status(201).json({
            ...createdMajorGroup.toObject(),
            id: createdMajorGroup._id,
          });
        })
        .catch(error => {
          res.status(500).json({
            message: "Updating major failed!",
            error: error
          });
        });
    })
    .catch(error => {
      res.status(500).json({
        message: "creating a MajorGroup failed!",
        error: error
      });
    });
}

// Updates MajorGroup
exports.updateMajorGroup = (req, res) => {
  const majorGroup = new MajorGroup({
    _id: req.params.groupId,
    parentMajor: req.body.parentMajor,
    type: req.body.type,
    mode: req.body.mode,
    year: req.body.year,
    semester: req.body.semester,
    beginYear: req.body.beginYear,
    students: req.body.students,
  });

  majorGroup.studentGroups = {};

  if (majorGroup.students.length) {
    majorGroup.studentGroups.labGroups = Math.ceil(majorGroup.students.length / 15);
    majorGroup.studentGroups.regularGroups = Math.ceil(majorGroup.students.length / 30);
  }

  MajorGroup.updateOne({
      _id: req.params.groupId
    }, majorGroup)
    .then((result) => {
      MajorGroup
        .findOne({
          _id: majorGroup._id
        })
        .populate('students')
        .exec((error, fetchedGroup) => {
          if (fetchedGroup && fetchedGroup.students.length) {
            fetchedGroup.students.sort((a, b) => {
              if (a.lastName < b.lastName)
                return -1;
              if (a.lastName > b.lastName)
                return 1;
              return 0;
            });

            let grLab = 1;
            let gr = 1;
            fetchedGroup.students.forEach((st, i) => {
              if (i !== 0 && i % 15 === 0) grLab++;
              if (i !== 0 && i % 30 === 0) gr++;

              Student.updateOne({
                  _id: st._id
                }, {
                  $set: {
                    classGroup: {
                      labGroup: grLab,
                      regularGroup: gr
                    }
                  }
                }).then((result) => {
                  console.log('student updated');
                })
                .catch(error => {
                  res.status(500).json({
                    message: "Updating student failed!",
                    error: error
                  });
                });
            });
          } else if (error) {
            res.status(500).json({
              message: 'Fetching Group failed!',
              error: error
            });
          }
        });
      return majorGroup;
    })
    .then((majorGroup) => {
      if (result.n > 0) {
        res.status(200).json({
          ...majorGroup.toObject(),
          id: majorGroup._id,
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update MajorGroup!',
        error: error
      });
    });
}

// Get all MajorGroups
exports.getMajorGroups = (req, res) => {
  MajorGroup.find({
      parentMajor: req.params.id
    })
    .select('-__v')
    .populate({
      path: 'parentMajor',
      select: 'name _id'
    })
    .exec((error, fetchedMajorGroups) => {
      if (fetchedMajorGroups) {
        res.status(200).json(fetchedMajorGroups);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching MajorGroups failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: 'MajorGroups not found!'
        });
      }
    });
}

// Get MajorGroup by id
exports.getMajorGroup = (req, res) => {
  MajorGroup.findById(req.params.groupId)
    .populate('students')
    .populate({
      path: 'parentMajor',
      select: 'name _id'
    })
    .exec((error, majorGroup) => {
      if (majorGroup) {
        res.status(200).json({
          ...majorGroup.toObject(),
          id: majorGroup._id
        });
      } else if (error) {
        res.status(500).json({
          message: 'Fetching MajorGroup failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: "MajorGroup not found!"
        });
      }
    });
}

// Deletes MajorGroup
exports.deleteMajorGroup = (req, res) => {
  MajorGroup.deleteOne({
      _id: req.params.groupId
    })
    .then((result) => {

      // todo: remove
      res.status(200).json(result);

      if (result.n > 0) {
        res.status(200).json({
          message: "MajorGroup deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting MajorGroup failed!',
        error: error
      });
    });
}
