const Teacher = require('../models/teacher');
const Room = require('../models/room');
const Schedule = require('../models/schedule');

// Creates Teacher
exports.createTeacher = (req, res) => {
  const teacher = new Teacher({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    title: req.body.title,
    office: req.body.office,
    departmentId: req.body.departmentId
  });

  teacher.save().then(createdTeacher => {
      const schedule = new Schedule({
        teacherId: createdTeacher._id,
        days: getEmptySchedule()
      });

      schedule.save()
        .then((result) => {
          const updatedTeacher = {
            ...createdTeacher.toObject(),
            schedule: result._id
          }

          Teacher.updateOne({
              _id: createdTeacher._id
            }, updatedTeacher)
            .then((result) => {
              res.status(201).json({
                ...createdTeacher.toObject(),
                scheduleId: updatedTeacher.schedule,
                id: createdTeacher._id,
              });
            }).catch(error => {
              res.status(500).json({
                message: "Updating teacher failed!",
                error: error
              });
            });

        }).catch(error => {
          res.status(500).json({
            message: "Creating a teacher schedule failed!",
            error: error
          });
        });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a Teacher failed!",
        error: error
      });
    });
}

function getEmptySchedule() {
  return [{
      dayType: 'MONDAY',
      classes: []
    },
    {
      dayType: 'TUESDAY',
      classes: []
    },
    {
      dayType: 'WEDNESDAY',
      classes: []
    },
    {
      dayType: 'THURSDAY',
      classes: []
    },
    {
      dayType: 'FRIDAY',
      classes: []
    },
  ];
}

// Creates many Teachers at once
exports.createManyTeachers = (req, res) => {

  req.body.teachers.forEach((t) => {
    const teacher = new Teacher({
      firstName: t.firstName,
      lastName: t.lastName,
      title: t.title,
      office: t.office,
      departmentId: t.departmentId
    });

    teacher.save()
      .then(createdTeacher => {
        console.log('created', createdTeacher);
      })
      .catch(error => {
        res.status(500).json({
          message: "Creating a Student failed!",
          error: error
        });
      });
  });

  res.status(201).json({
    message: 'Operation succeed'
  });
}

// Updates many Teachers at once
exports.updateManyTeachers = (req, res) => {

  req.body.teachers.forEach((t) => {
    const schedule = new Schedule({
      teacherId: t._id,
      days: getEmptySchedule()
    });

    schedule.save()
      .then((result) => {
        const teacher = new Teacher({
          _id: t._id,
          firstName: t.firstName,
          lastName: t.lastName,
          title: t.title,
          office: t.office,
          departmentId: t.departmentId,
          schedule: result._id
        });

        Teacher.updateOne({
              _id: teacher._id
            },
            teacher
          ).then(createdTeacher => {
            console.log('created', createdTeacher);
          })
          .catch(error => {
            res.status(500).json({
              message: "Creating a teacher failed!",
              error: error
            });
          });
      });
  });

  res.status(201).json({
    message: 'Operation succeed'
  });
}

// Updates Teacher
exports.updateTeacher = (req, res) => {
  const teacher = new Teacher({
    _id: req.params.id,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    title: req.body.title,
    office: req.body.office,
  });

  Teacher.updateOne({
      _id: req.params.id
    }, teacher)
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          ...teacher.toObject(),
          id: teacher._id
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Teacher!',
        error: error
      });
    });
}

// Get all teachers
exports.getTeachers = (req, res) => {
  Teacher.find({
      departmentId: req.params.departmentId
    })
    .select('-courses -__v')
    .populate({
      path: 'office',
      select: '-departmentId -__v'
    })
    .populate({
      path: 'schedule',
      populate: {
        path: 'days.classes',
        model: 'Class'
      }})
    .exec((error, fetchedTeachers) => {
      if (fetchedTeachers) {
        Room.populate(fetchedTeachers, {
          path: 'office.building',
          select: 'name',
          model: 'Building'
        }, (error, fetchedTeachers) => {
          if (fetchedTeachers) {
            res.status(200).json(fetchedTeachers);
          } else if (error) {
            res.status(500).json({
              message: 'Fetching Teachers failed!',
              error: error
            });
          }
        });
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Teachers failed!',
          error: error
        });
      }
    });
}

// Get teacher by id
exports.getTeacher = (req, res) => {

  Teacher.findById(req.params.id)
    .then((teacher) => {
      if (teacher) {
        res.status(200).json({
          ...teacher.toObject(),
          id: teacher._id
        });
      } else {
        res.status(400).json({
          message: "Teacher not found!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching Teacher failed!',
        error: error
      });
    });
}

// Deletes teacher
exports.deleteTeacher = (req, res) => {
  Teacher.deleteOne({
      _id: req.params.id
    })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: "Teacher deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting Teacher failed!',
        error: error
      });
    });
}
