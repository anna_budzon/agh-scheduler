const Room = require('../models/room');
const Building = require('../models/building');
const Schedule = require('../models/schedule');

// Creates Room
exports.createRoom = (req, res) => {
  const room = new Room({
    number: req.body.number,
    building: req.body.building,
    type: req.body.type,
    departmentId: req.body.departmentId,
    limitOfPlaces: req.body.limitOfPlaces
  });

  room.save().then(createdRoom => {
      const schedule = new Schedule({
        roomId: createdRoom._id,
        days: getEmptySchedule()
      });

      Promise.all([Building.updateOne({
          _id: createdRoom.building
        }, {
          $push: {
            rooms: createdRoom
          }
        }),
        schedule.save()]
      ).then(result => {
        const updatedRoom = {
          ...createdRoom.toObject(),
          schedule: result[1]._id
        }
        Room.updateOne({
          _id: createdRoom._id
        }, updatedRoom)
          .then((result) => {
            res.status(201).json({
              ...createdRoom.toObject(),
              scheduleId: updatedRoom.schedule,
              id: createdRoom._id,
            });
          }).catch(error => {
            res.status(500).json({
              message: "Updating room failed!",
              error: error
            });
          });

      }).catch(error => {
        res.status(500).json({
          message: 'Could not update building or schedule!',
          error: error
        });
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a Room failed!",
        error: error
      });
    });
}

function getEmptySchedule() {
  return [{
      dayType: 'MONDAY',
      classes: []
    },
    {
      dayType: 'TUESDAY',
      classes: []
    },
    {
      dayType: 'WEDNESDAY',
      classes: []
    },
    {
      dayType: 'THURSDAY',
      classes: []
    },
    {
      dayType: 'FRIDAY',
      classes: []
    },
  ];
}


// Updates many Rooms at once
exports.updateManyRooms = (req, res) => {

  req.body.rooms.forEach((r) => {
    const schedule = new Schedule({
      roomId: r._id,
      days: getEmptySchedule()
    });

    schedule.save()
      .then((result) => {
        const room = new Room({
          _id: r._id,
          number: r.number,
          building: r.building,
          type: r.type,
          departmentId: r.departmentId,
          limitOfPlaces: r.limitOfPlaces,
          schedule: result._id
        });

        Room.updateOne({
              _id: room._id
            },
            room
          ).then(createdRoom => {
            console.log('created', createdRoom);
          })
          .catch(error => {
            res.status(500).json({
              message: "Creating a room failed!",
              error: error
            });
          });
      });
  });

  res.status(201).json({
    message: 'Operation succeed'
  });
}

// Updates Room
exports.updateRoom = (req, res) => {
  const room = new Room({
    _id: req.params.id,
    number: req.body.number,
    building: req.body.building,
    type: req.body.type,
    departmentId: req.body.departmentId,
    limitOfPlaces: req.body.limitOfPlaces,
    schedule: req.body.schedule
  });

  Room.updateOne({
      _id: req.params.id
    }, room)
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          ...room.toObject(),
          id: room._id
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Room!',
        error: error
      });
    });
}

// Get all rooms
exports.getRooms = (req, res) => {
  Room.find({
      departmentId: req.params.departmentId
    })
    .select('-__v')
    .populate({
      path: 'building',
      select: 'name'
    })
    .populate({
      path: 'schedule',
      populate: {
        path: 'days.classes',
        model: 'Class'
      }})
    .exec((error, fetchedRooms) => {
      if (fetchedRooms) {
        res.status(200).json(fetchedRooms);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Rooms failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: 'Rooms not found!'
        });
      }
    });
}

// Get room by id
exports.getRoom = (req, res) => {

  Room.findById(req.params.id)
    .populate({
      path: 'building',
      select: 'name'
    })
    .populate({
      path: 'schedule',
      populate: {
        path: 'days.classes',
        model: 'Class'
      }})
    .exec((error, room) => {
      if (room) {
        res.status(200).json({
          ...room.toObject(),
          id: room._id
        });
      } else if (error) {
        res.status(500).json({
          message: "Fetching room failed!",
          error: error
        });
      } else {
        res.status(400).json({
          message: "Room not found!"
        });
      }
    });
}

// Deletes room
exports.deleteRoom = (req, res) => {
  Room.deleteOne({
      _id: req.params.id
    })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: "Room deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting Room failed!',
        error: error
      });
    });
}
