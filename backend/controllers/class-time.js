const ClassTime = require('../models/class-time');

// Creates ClassTime
exports.createClassTime = (req, res) => {
  const classTime = new ClassTime({
    day: req.body.day,
    from: req.body.from,
    to: req.body.to,
    length: req.body.length
  });

  classTime.save().then(createdClassTime => {
      res.status(201).json({
        ...createdClassTime.toObject(),
        id: createdClassTime._id,
      });
  })
  .catch(error => {
    res.status(500).json({
      message: "Creating a ClassTime failed!",
      error: error
    });
  });
}

// Updates ClassTime
exports.updateClassTime = (req, res) => {
  const classTime = new ClassTime({
      _id: req.params.id,
      day: req.body.day,
      from: req.body.from,
      to: req.body.to,
      length: req.body.length
  });

  ClassTime.updateOne({ _id: req.params.id }, classTime)
      .then((result) => {
        if(result.n > 0){
          res.status(200).json({
            ...classTime.toObject(),
            id: classTime._id
          });
        } else {
          res.status(401).json({
            message: "Not authorized!"
          });
        }
      })
      .catch(error => {
        res.status(500).json({
          message: 'Could not update ClassTime!',
          error: error
        });
      });
}

// Get classTime by id
exports.getClassTime = (req, res) => {

  ClassTime.findById(req.params.id)
      .then((classTime) => {
          if (classTime) {
            res.status(200).json({
              ...classTime.toObject(),
              id: classTime._id
            });
          } else {
            res.status(400).json({
                message: "ClassTime not found!"
            });
          }
      })
      .catch(error => {
        res.status(500).json({
          message: 'Fetching ClassTime failed!',
          error: error
        });
      });
}

// Deletes classTime
exports.deleteClassTime = (req, res) => {
  ClassTime.deleteOne({ _id: req.params.id })
    .then((result) => {
      if(result.n > 0){
        res.status(200).json({
            message: "ClassTime deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting ClassTime failed!',
        error: error
      });
    });
}
