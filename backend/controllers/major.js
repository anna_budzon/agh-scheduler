const Major = require('../models/major');
const Department = require('../models/department');

// Creates Major
exports.createMajor = (req, res) => {
  const major = new Major({
    name: req.body.name,
    departmentId: req.body.departmentId,
    majorType: req.body.majorType,
    majorMode: req.body.majorMode,
    groups: req.body.groups
  });

  major.save().then(createdMajor => {
      res.status(201).json({
        ...createdMajor.toObject(),
        id: createdMajor._id
      });
      return createdMajor;
    })
    .then((createdMajor) => {
      Department.updateOne({
        _id: req.body.departmentId
      }, {
        $push: {
          majors: createdMajor._id
        }
      })
      .then((result) => {
        console.log(result);
      })
      .catch(error => {
        res.status(500).json({
          message: "Updating department failed!",
          error: error
        });
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a major failed!",
        error: error
      });
    });
}

// Updates Major
exports.updateMajor = (req, res) => {
  const major = new Major({
    _id: req.params.id,
    departmentId: req.body.departmentId,
    name: req.body.name,
    majorType: req.body.majorType,
    majorMode: req.body.majorMode,
    groups: req.body.groups
  });

  Major.updateOne({
      _id: req.params.id
    }, major)
    .then((result) => {

      // todo: delete
      res.status(200).json({
        ...major.toObject(),
        id: major._id
      });

      if (result.n > 0) {
        res.status(200).json(major);
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Major!',
        error: error
      });
    });
}

// Get all Majors
exports.getMajors = (req, res) => {
  Major.find({
      departmentId: req.params.departmentId
    })
    .select('-groups -__v')
    .exec((error, fetchedMajors) => {
      if (fetchedMajors) {
        res.status(200).json(fetchedMajors);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Majors failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: 'Majors not found!'
        });
      }
    });
}

// Get Major by id
exports.getMajor = (req, res) => {
  Major.findById(req.params.id)
    .populate('groups')
    .exec((error, major) => {
      if (major) {
        res.status(200).json({
          ...major.toObject(),
          id: major._id
        });
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Major failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: "Major not found!"
        });
      }
    });
}

// Deletes Major
exports.deleteMajor = (req, res) => {

  Department.updateOne({
    _id: req.body.departmentId
  }, {
    $pop: {
      majors: major
    }
  })
  .then((result) => {
    console.log(result);
  })
  .catch(error => {
    res.status(500).json({
      message: "Updating department failed!",
      error: error
    });
  });

  Major.deleteOne({
      _id: req.params.id
    })
    .then((result) => {
      // todo: remove
      res.status(200).json(result);

      if (result.n > 0) {
        res.status(200).json({
          message: "Major deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting major failed!',
        error: error
      });
    });
}
