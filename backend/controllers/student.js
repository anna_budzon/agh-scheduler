const Student = require('../models/student');
const MajorGroup = require('../models/major-group');

// Creates student
exports.createStudent = (req, res) => {
  const student = new Student({
    index: req.body.index,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    pesel: req.body.pesel,
    groupMajorId: req.body.groupMajorId,
    major: req.body.major,
    departmentId: req.body.departmentId
  });

  student.save().then(createdStudent => {
      res.status(201).json({
        ...createdStudent.toObject(),
        id: createdStudent._id
      });
      return createdStudent;
    })
    .then((createdStudent) => {
      MajorGroup.updateOne({
          _id: req.body.groupMajorId
        }, {
          $push: {
            students: createdStudent.id
          }
        })
        .then((result) => {
          console.log(result);
        })
        .catch(error => {
          res.status(500).json({
            message: "Updating MajorGroup failed!",
            error: error
          });
        });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a Student failed!",
        error: error
      });
    });
}

// Creates many students at once
exports.createManyStudents = (req, res) => {

  req.body.students.forEach((s) => {
    const student = new Student({
      index: s.index,
      firstName: s.firstName,
      lastName: s.lastName,
      pesel: s.pesel,
      groupMajorId: s.groupMajorId,
      major: s.major,
      departmentId: s.departmentId
    });

    student.save()
      .then((createdStudent) => {
        MajorGroup.updateOne({
            _id: s.groupMajorId
          }, {
            $push: {
              students: createdStudent.id
            }
          })
          .then((result) => {
            console.log(result);
          })
          .catch(error => {
            res.status(500).json({
              message: "Updating MajorGroup failed!",
              error: error
            });
          });
      })
      .catch(error => {
        res.status(500).json({
          message: "Creating a Student failed!",
          error: error
        });
      });
  });

  res.status(201).json({
    message: 'Operation succeed'
  });
}

// Updates student
exports.updateStudent = (req, res) => {
  const student = new Student({
    _id: req.params.id,
    index: req.body.index,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    pesel: req.body.pesel,
    groupMajorId: req.body.groupMajorId,
    major: req.body.major,
    departmentId: req.body.departmentId
  });

  Student.updateOne({
      _id: req.params.id
    }, student)
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          ...student.toObject(),
          id: student._id
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Student!',
        error: error
      });
    });
}

// Get students of one major
exports.getStudents = (req, res) => {
  Student.find({
      groupMajorId: req.params.majorGroupId
    })
    .populate({ path:'major', select:'name _id' })
    .exec((error, fetchedStudents) => {
      if (fetchedStudents) {
        res.status(200).json(fetchedStudents);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Students failed!',
          error: error
        });
      } else {
        res.status(40).json({
          message: 'Students not found!'
        });
      }
    });
}

// Get all students
exports.getAllStudents = (req, res) => {
  Student.find({
      departmentId: req.params.departmentId
    })
    .populate({ path:'major', select:'name _id' })
    .exec((error, fetchedStudents) => {
      if (fetchedStudents) {
        res.status(200).json(fetchedStudents);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Students failed!',
          error: error
        });
      } else {
        res.status(40).json({
          message: 'Students not found!'
        });
      }
    });
}

// Get student by id
exports.getStudent = (req, res) => {

  Student.findById(req.params.id)
    .then((student) => {
      if (student) {
        res.status(200).json({
          ...student.toObject(),
          id: student._id
        });
      } else {
        res.status(400).json({
          message: "Student not found!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching Student failed!',
        error: error
      });
    });
}

// Deletes student
exports.deleteStudent = (req, res) => {
  Student.deleteOne({
      _id: req.params.id
    })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: "Student deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting Student failed!',
        error: error
      });
    });
}
