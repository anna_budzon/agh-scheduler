const Building = require('../models/building');

// Creates Building
exports.createBuilding = (req, res) => {
  const building = new Building({
    name: req.body.name,
    rooms: req.body.rooms
  });

  building.save().then(createdBuilding => {
      res.status(201).json({
        ...createdBuilding.toObject(),
        id: createdBuilding._id,
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a Building failed!",
        error: error
      });
    });
}

// Updates Building
exports.updateBuilding = (req, res) => {
  const building = new Building({
    _id: req.params.id,
    name: req.body.name,
    rooms: req.body.rooms
  });

  Building.updateOne({
      _id: req.params.id
    }, building)
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          ...building.toObject(),
          id: building._id
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Building!',
        error: error
      });
    });
}

// Get all buildings
exports.getBuildings = (req, res) => {
  Building.find({})
    .populate('rooms')
    .exec((error, fetchedBuildings) => {
      if (fetchedBuildings) {
        res.status(200).json(fetchedBuildings);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Buildings failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: 'Buildings not found!'
        });
      }
    });
}

// Deletes building
exports.deleteBuilding = (req, res) => {
  Building.deleteOne({
      _id: req.params.id
    })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: "Building deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting Building failed!',
        error: error
      });
    });
}
