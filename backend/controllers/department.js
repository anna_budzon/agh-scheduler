const Department = require('../models/department');

// Creates Department
exports.createDepartment = (req, res, next) => {
  const department = new Department({
      name: req.body.name,
      majors: req.body.majors
  });

  department.save().then(createdDepartment => {
      res.status(201).json({
        ...createdDepartment.toObject(),
        id: createdDepartment._id,
      });
  })
  .catch(error => {
    res.status(500).json({
      message: "Creating a Department failed!",
      error: error
    });
  });
}

// Updates Department
exports.updateDepartment = (req, res) => {
  const department = new Department({
      _id: req.params.id,
      name: req.body.name,
      majors: req.body.majors
  });

  Department.updateOne({ _id: req.params.id }, department)
      .then((result) => {

        // todo: remove
        res.status(200).json({
          message: "Department updated!",
          department: result
      });

        if(result.n > 0){
          res.status(200).json({ ...result.toObject(), id: result._id });
        } else {
          res.status(401).json({
            message: "Not authorized!"
          });
        }
      })
      .catch(error => {
        res.status(500).json({
          message: 'Could not update Department!',
          error: error
        });
      });
}

// Get Department by id
exports.getDepartment = (req, res) => {

  Department.findById(req.params.id)
      .populate('majors')
      .exec((error, department) => {
          if (department) {
              res.status(200).json({
                ...department.toObject(),
                id: department._id
              });
          } else if (error) {
            res.status(500).json({
              message: 'Fetching Department failed!',
              error: error
            });
          } else {
              res.status(400).json({
                  message: "Department not found!"
              });
          }
      });
}

// Deletes Department
exports.deleteDepartment = (req, res) => {
  Department.deleteOne({
    _id: req.params.id,
    })
      .then((result) => {
        if(result.n > 0){
          res.status(200).json({
              message: "Department deleted!"
          });
        } else {
          res.status(401).json({
            message: "Not authorized!"
          });
        }
      })
      .catch(error => {
        res.status(500).json({
          message: 'Deleting department failed!',
          error: error
        });
      });
}
