const Schedule = require('../models/schedule');
const Class = require('../models/class');
const Teacher = require('../models/teacher');
const Room = require('../models/room');
const MajorGroup = require('../models/major-group');


// Creates Schedule
exports.initializeSchedule = (req, res) => {
  const schedule = new Schedule({
    major: req.body.major,
    majorGroup: req.body.majorGroup,
    days: req.body.days,
    completed: false
  });

  schedule.save()
    .then(createdSchedule => {

      MajorGroup.updateOne({
        _id: req.body.majorGroup
      }, {
        $set: {
          schedule: createdSchedule._id
        }
      }).then((result) => {
        console.log(result);
        res.status(201).json({
          ...createdSchedule.toObject(),
          id: createdSchedule._id,
        });
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a Schedule failed!",
        error: error
      });
    });
}

// Create an initialized Schedule
exports.createSchedule = (req, res) => {
  Promise.all(req.body.days.map(day => {
      return Promise.all(day.classes.map(cl => {
        const newClass = new Class({
          name: cl.name,
          course: cl.course,
          type: cl.type,
          time: cl.time,
          room: cl.room,
          regularity: cl.regularity,
          teacher: cl.teacher,
          scheduleId: cl.scheduleId,
          studentGroupNr: cl.studentGroupNr
        });

        return newClass.save();
      }))
    })).then((createdClasses) => {
      const classes = [];
      createdClasses.forEach((dayClasses, i) => {

        dayClasses.forEach(cls => {
          classes.push({
            dayType: getDayType(i),
            classId: cls._id,
            roomId: cls.room,
            teacherId: cls.teacher
          });
        });
      });
      updateAllSchedules(classes, req.params.id)
        .then((result) => {
          res.status(200).json({
            message: 'Schedule saved, classes saved, operation succeeded!'
          });
        })
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Schedule!',
        error: error
      });
    });
}

function getDayType(idx) {
  switch (idx) {
    case 0:
      return 'MONDAY';
    case 1:
      return 'TUESDAY';
    case 2:
      return 'WEDNESDAY';
    case 3:
      return 'THURSDAY';
    case 4:
      return 'FRIDAY';
  }
}

function updateAllSchedules(classes, id, res) {

  if (!classes || !classes.length) {
    res.status(500).json({
      message: 'Could not update schedules! No class found!',
      error: error
    });
  }
  return Promise.all(
    classes.map((cls) => {
      return Promise.all([
        Schedule.updateOne({
          _id: id,
          'days.dayType': cls.dayType
        }, {
          $push: {
            'days.$.classes': cls.classId
          },
          $set: {
            completed: true
          }
        }).catch(error => {
          res.status(500).json({
            message: 'Could not update schedule for group!',
            error: error
          });
        }),
        Schedule.updateOne({
          teacherId: cls.teacherId,
          'days.dayType': cls.dayType
        }, {
          $push: {
            'days.$.classes': cls.classId
          }
        }).catch(error => {
          res.status(500).json({
            message: 'Could not update schedule for teacher!',
            error: error
          });
        }),
        Schedule.updateOne({
          roomId: cls.roomId,
          'days.dayType': cls.dayType
        }, {
          $push: {
            'days.$.classes': cls.classId
          }
        }).catch(error => {
          res.status(500).json({
            message: 'Could not update schedule for room!',
            error: error
          });
        })
      ]);
    }));
}

// Update a Schedule
exports.updateSchedule = (req, res) => {

  Schedule.findOne({
      _id: req.params.id
    })
    .then((schedule) => {
      clearSchedule(req.params.id, schedule, res)
        .then(() => {
          Promise.all(req.body.days.map(day => {
              return Promise.all(day.classes.map(cl => {
                const newClass = new Class({
                  name: cl.name,
                  course: cl.course,
                  type: cl.type,
                  time: cl.time,
                  room: cl.room,
                  regularity: cl.regularity,
                  teacher: cl.teacher,
                  scheduleId: cl.scheduleId,
                  studentGroupNr: cl.studentGroupNr
                });

                return newClass.save();
              }))
            })).then((createdClasses) => {
              const classes = [];
              createdClasses.forEach((dayClasses, i) => {

                dayClasses.forEach(cls => {
                  classes.push({
                    dayType: getDayType(i),
                    classId: cls._id,
                    roomId: cls.room,
                    teacherId: cls.teacher
                  });
                });
              });
              updateAllSchedules(classes, req.params.id, res)
                .then((result) => {
                  res.status(200).json({
                    message: 'Schedule updated, classes saved, operation succeeded!'
                  });
                })
            })
            .catch(error => {
              res.status(500).json({
                message: 'Could not update Schedule!',
                error: error
              });
            });
        })
        .catch(error => {
          res.status(500).json({
            message: 'Could not clear Schedule!',
            error: error
          });
        });
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not fetch Schedule!',
        error: error
      });
    });
}

function clearSchedule(id, schedule, res) {
  return Class.deleteMany({
      scheduleId: id
    })
    .then((deleted) => {
      console.log('classes deleted', deleted);

      const deletedClasses = [];
      schedule.days.forEach(d =>
        d.classes.map(c => deletedClasses.push(c))
      );
      return deleteFromSchedules(deletedClasses, id, res);
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Schedule!',
        error: error
      });
    });
}

function deleteFromSchedules(classes, id, res) {
  return Promise.all(
    classes.map((cls) => {
      console.log(cls);
      console.log(id);
      return Promise.all([
        Schedule.updateOne({
          _id: id,
          'days.dayType': cls.dayType
        }, {
          $pull: {
            'days.$.classes': cls
          },
          $set: {
            completed: false
          }
        }).catch(error => {
          res.status(500).json({
            message: 'Could not update schedule for group!',
            error: error
          });
        }),
        Schedule.updateOne({
          teacherId: cls.teacherId,
          'days.dayType': cls.dayType
        }, {
          $pull: {
            'days.$.classes': cls
          }
        }).catch(error => {
          res.status(500).json({
            message: 'Could not update schedule for teacher!',
            error: error
          });
        }),
        Schedule.updateOne({
          roomId: cls.roomId,
          'days.dayType': cls.dayType
        }, {
          $pull: {
            'days.$.classes': cls
          }
        }).catch(error => {
          res.status(500).json({
            message: 'Could not update schedule for room!',
            error: error
          });
        })
      ]);
    }));
}

// Update teacher/room preferences
exports.updateSchedulePreferences = (req, res) => {

  let occupiedClasses = [];
  req.body.days.map(d => {
    const tempCls = d.classes.filter(c => !c._id);
    occupiedClasses = occupiedClasses.concat(tempCls);
  });
  Promise.all(occupiedClasses.map(cls => {
    const newClass = new Class({
      time: cls.time,
      scheduleId: cls.scheduleId
    });
    return newClass.save();
  }))
    .then((result) => {

      req.body.days.map(d => {
        result.forEach(cls => {
         const occupiedCls =  d.classes.find(c => (c.time.id === cls.time.id) && c.time.day === d.dayType);
         if (occupiedCls) {
           const idx = d.classes.indexOf(occupiedCls);
           d.classes.splice(idx, 1, cls);
          }
        })
      });
      Schedule.findOneAndUpdate({
        _id: req.params.id
      }, req.body)
      .then((result) => {
        res.status(200).json({
          ...result.toObject(),
          id: result._id
        });
      })
      .catch(error => {
        res.status(500).json({
          message: 'Could not update schedule!',
          error: error
        });
      });
    });
}

// Deletes schedule
exports.deleteSchedule = (req, res) => {
  Schedule.findOne({
      _id: req.params.id
    })
    .then((schedule) => {
      clearSchedule(req.params.id, schedule)
        .then((deleted) => {
          Schedule.deleteOne({
              _id: req.params.id
            })
            .then((result) => {
              if (result.n > 0) {
                res.status(200).json({
                  message: "Schedule deleted!"
                });
              } else {
                res.status(401).json({
                  message: "Not authorized!"
                });
              }
            })
            .catch(error => {
              res.status(500).json({
                message: 'Deleting Schedule failed!',
                error: error
              });
            });
        }).catch(error => {
          res.status(500).json({
            message: 'Clearing Schedule failed!',
            error: error
          });
        });
    }).catch(error => {
      res.status(500).json({
        message: 'Fetching Schedule failed!',
        error: error
      });
    });
}


// Get schedules
exports.getSchedules = (req, res) => {
  Schedule.find({
      departmentId: req.params.departmentId
    })
    .populate({
      path: 'days',
      populate: {
        path: 'classes',
        model: 'Class'
      }
    })
    .exec((error, fetchedSchedules) => {
      if (fetchedSchedules) {
        res.status(200).json(fetchedSchedules);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Schedules failed!',
          error: error
        });
      }
    });
}

// Get schedule by id
exports.getSchedule = (req, res) => {

  Schedule.findById(req.params.id)
    .populate({
      path: 'days.classes',
      model: 'Class',
      populate: [{
          path: 'room',
          model: 'Room',
          populate: {
            path: 'building',
            select: 'name',
            model: 'Building'
          }
        },
        {
          path: 'teacher',
          model: 'Teacher'
        }
      ]
    })
    .populate({
      path: 'majorGroup',
      select: 'beginYear semester type mode'
    })
    .populate({
      path: 'major',
      select: 'name'
    })
    .exec((error, fetchedSchedule) => {
      if (fetchedSchedule) {
        res.status(200).json({
          ...fetchedSchedule.toObject(),
          id: fetchedSchedule._id
        });
      } else if (error) {
        res.status(500).json({
          message: "Fetching schedule failed!"
        });
      } else {
        res.status(400).json({
          message: "Schedule not found!"
        });
      }
    });
}

// Check if room, group and teacher are available in the given time.
exports.checkAvailability = (req, res) => {

  const teacherAvailability = defineTeacherAvailability(
    req.body.teacher, createResponse(), req.body.teacher.schedule
  );
  const fullAvailability = defineRoomAvailability(
    req.body.room, teacherAvailability, req.body.room.schedule
  );
  res.status(200).json({
    schedule: fullAvailability
  });

  // Schedule.populate(req.body.teacher, {
  //   path: 'schedule',
  //   select: 'days',
  //   populate: {
  //     path: 'classes',
  //     model: 'Class'
  //   }
  // }).exec((error, fetchedTeacherSchedule) => {
  //   if (fetchedTeacherSchedule) {
  //     const teacherAvailability = defineTeacherAvailability(
  //       fetchedTeacher, createResponse(), fetchedTeacherSchedule
  //     );
  //     return {
  //       availability: teacherAvailability,
  //       room: req.body.room
  //     };
  //   } else if (error) {
  //     res.status(500).json({
  //       message: "Fetching teachers schedule failed!",
  //       error: error
  //     });
  //   }
  // }).then((data) => {
  //   Schedule.populate(data.room, {
  //       path: 'schedule',
  //       select: 'days',
  //       populate: {
  //         path: 'classes',
  //         model: 'Class',
  //         populate: [{
  //           path: 'time',
  //           model: 'ClassTime'
  //         }]
  //       }
  //     })
  //     .exec((error, fetchedRoomSchedule) => {
  //       if (fetchedRoomSchedule) {
  //         const fullAvailability = defineRoomAvailability(
  //           data.room, data.availability, fetchedRoomSchedule
  //         );
  //         res.status(200).json(fullAvailability);
  //       } else if (error) {
  //         res.status(500).json({
  //           message: "Fetching room schedule failed!",
  //           error: error
  //         });
  //       }
  //     });

  // });




  // Teacher.findById(req.body.teacher.id)
  //   .populate('schedule')
  //   .exec((error, fetchedTeacher) => {
  //     if (fetchedTeacher) {
  //       Schedule.populate(fetchedTeacher, {
  //           path: 'schedule',
  //           select: 'days',
  //           populate: {
  //             path: 'classes',
  //             model: 'Class',
  //             populate: [{
  //               path: 'time',
  //               model: 'ClassTime'
  //             }]
  //           }
  //         })
  //         .exec((error, fetchedTeacherSchedule) => {
  //           if (fetchedTeacherSchedule) {
  //             // const availabilityResponse = createResponse();
  //             const teacherAvailability = defineTeacherAvailability(
  //               fetchedTeacher, createResponse(), fetchedTeacherSchedule
  //             );
  //             return teacherAvailability;
  //           } else if (error) {
  //             res.status(500).json({
  //               message: "Fetching teachers schedule failed!",
  //               error: error
  //             });
  //           }
  //         })
  //         .then((availability) => {
  //           Room.findById(req.body.room.id)
  //             .populate('schedule')
  //             .populate('building')
  //             .exec((error, fetchedRoom) => {
  //               if (fetchedRoom) {

  //                 Schedule.populate(fetchedRoom, {
  //                     path: 'schedule.days',
  //                     populate: {
  //                       path: 'classes',
  //                       model: 'Class',
  //                       populate: [{
  //                         path: 'time',
  //                         model: 'ClassTime'
  //                       }]
  //                     }
  //                   })
  //                   .exec((error, fetchedRoomSchedule) => {
  //                     if (fetchedRoomSchedule) {
  //                       // const availabilityResponse = createResponse();
  //                       const fullAvailability = defineRoomAvailability(
  //                         fetchedRoom, availability, fetchedRoomSchedule
  //                       );
  //                       res.status(200).json(fullAvailability);
  //                     } else if (error) {
  //                       res.status(500).json({
  //                         message: "Fetching room schedule failed!",
  //                         error: error
  //                       });
  //                     }
  //                   });

  //               } else if (error) {
  //                 res.status(500).json({
  //                   message: "Fetching room failed!",
  //                   error: error
  //                 });
  //               } else {
  //                 res.status(404).json({
  //                   message: "Room not found!",
  //                 });
  //               }
  //             })
  //         });
  //     } else if (error) {
  //       res.status(500).json({
  //         message: "Fetching teacher failed!",
  //         error: error
  //       });
  //     } else {
  //       res.status(404).json({
  //         message: "Teacher not found!",
  //       });
  //     }
  //   });
}

function createResponse() {
  const availability = {
    MONDAY: [],
    TUESDAY: [],
    WEDNESDAY: [],
    THURSDAY: [],
    FRIDAY: [],
    SATURDAY: [],
    SUNDAY: []
  };

  for (let i = 0; i < 7; i++) {
    availability.MONDAY.push({
      id: i,
      state: 2,
      conflicts: []
    });
    availability.TUESDAY.push({
      id: i,
      state: 2,
      conflicts: []
    });
    availability.WEDNESDAY.push({
      id: i,
      state: 2,
      conflicts: []
    });
    availability.THURSDAY.push({
      id: i,
      state: 2,
      conflicts: []
    });
    availability.FRIDAY.push({
      id: i,
      state: 2,
      conflicts: []
    });
  }
  return availability;
}

function defineTeacherAvailability(teacher, availability, schedule) {
  if (!schedule || !schedule.days || !schedule.days.length) {
    return availability;
  }

  schedule.days.forEach(day => {
    const avDay = availability[day.dayType];

    if (!day.classes.length) {
      return;
    }

    const classesTimesID = day.classes.map(cls => cls.time.id);

    classesTimesID.forEach(id => {
      const isOccupied = avDay.find(elem => elem.id === id);
      if (isOccupied) {
        const idx = avDay.indexOf(isOccupied);
        avDay.splice(idx, 1, {
          id: isOccupied.id,
          state: 1,
          conflicts: [{
            message: `Prowadzący ${teacher.title} ${teacher.lastName} ma w tym czasie zajęcia.`,
            conflictType: 1
          }]
        });
      } else {
        const idx = avDay.indexOf(isOccupied);
        avDay.splice(idx, 1, {
          id: isOccupied.id,
          state: 2
        });
      }
    })
  });

  return availability;
}

function defineRoomAvailability(room, availability, schedule) {
  if (!schedule || !schedule.days || !schedule.days.length) {
    return availability;
  }

  schedule.days.forEach(day => {
    const avDay = availability[day.dayType];

    if (!day.classes.length) {
      return;
    }

    const classesTimesID = day.classes.map(cls => cls.time.id);

    classesTimesID.forEach(id => {
      const isOccupied = avDay.find(elem => elem.id === id);
      if (isOccupied) {
        const idx = avDay.indexOf(isOccupied);
        if (isOccupied.state === 1) {
          isOccupied.conflicts.push({
            message: `Sala ${room.building.name}-${room.number} jest w tym czasie zajęta`,
            conflictType: 2
          });
        } else {
          avDay.splice(idx, 1, {
            id: isOccupied.id,
            state: 1,
            conflicts: [{
              message: `Sala ${room.building.name}-${room.number} jest w tym czasie zajęta`,
              conflictType: 2
            }]
          });
        }
      } else {
        const idx = avDay.indexOf(isOccupied);
        avDay.splice(idx, 1, {
          id: isOccupied.id,
          state: 2
        });
      }
    })
  });

  return availability;
}

// Creates Class of Schedule
exports.createClasses = (req, res) => {
  const cl = req.body ? req.body.classes : req;
  const newClass = new Class({
    name: cl.name,
    course: cl.course,
    type: cl.type,
    time: cl.time,
    room: cl.room,
    regularity: cl.regularity,
    teacher: cl.teacher,
    scheduleId: cl.scheduleId,
    studentGroupNr: cl.studentGroupNr
  });

  newClass.save()
    .then(createdClass => {
      return {
        ...createdClass.toObject(),
        id: createdClass._id,
      };
    })
    .catch(error => {
      return {
        error: error
      };
    });
}

// Updates Classes
exports.updateClasses = (req, res) => {
  const updatedClasses = [];
  req.body.classes.forEach(cl => {
    updatedClasses.push(new Class({
      id: cl.id,
      name: cl.name,
      course: cl.course,
      type: cl.type,
      time: cl.time,
      room: cl.room,
      regularity: cl.regularity,
      teacher: cl.teacher,
      scheduleId: cl.scheduleId,
      studentGroupNr: cl.studentGroupNr
    }));
  });

  const returnedClasses = [];

  updatedClasses.forEach((upClass) => {
    Schedule.updateOne({
        _id: upClass.id
      }, upClass)
      .then((result) => {
        if (result.n > 0) {
          returnedClasses.push({
            ...upClass.toObject(),
            id: upClass._id
          });
        } else {
          res.status(401).json({
            message: "Not authorized!"
          });
        }
      })
      .catch(error => {
        res.status(500).json({
          message: 'Could not update Schedule!',
          error: error
        });
      });
  });

  res.status(200).json(returnedClasses);
}

// Get classes
exports.getClasses = (req, res) => {
  Class.find({
      scheduleId: req.params.id
    })
    .populate('course')
    .populate('time')
    .populate('room')
    .populate('teacher')
    .exec((error, fetchedClasses) => {
      if (fetchedClasses) {
        Room.populate(fetchedClasses, {
          path: 'room.building',
          select: 'name',
          model: 'Building'
        }, (error, fetchedClasses) => {
          if (fetchedClasses) {
            res.status(200).json(fetchedClasses);
          } else if (error) {
            res.status(500).json({
              message: 'Fetching Classes failed!',
              error: error
            });
          } else {
            res.status(400).json({
              message: 'No class found!'
            });
          }
        });
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Classes failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: 'No class found!'
        });
      }
    });
}

// Get class by id
exports.getClass = (req, res) => {

  Class.findById(req.params.classId)
    .populate('course')
    .populate('time')
    .populate('room')
    .populate('teacher')
    .exec((error, fetchedClass) => {
      if (fetchedClass) {
        Room.populate(fetchedClass, {
          path: 'room.building',
          select: 'name',
          model: 'Building'
        }, (error, fetchedClass) => {
          if (fetchedClass) {
            res.status(200).json(fetchedClass);
          } else if (error) {
            res.status(500).json({
              message: 'Fetching Class failed!',
              error: error
            });
          }
        });
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Class failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: 'Class not found!'
        });
      }
    });
}
