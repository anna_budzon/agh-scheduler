const Course = require('../models/course');
const MajorGroup = require('../models/major-group');

// Creates Course
exports.createCourse = (req, res) => {
  const course = new Course({
    name: req.body.name,
    majorType: req.body.majorType,
    majorMode: req.body.majorMode,
    major: req.body.major,
    semester: req.body.semester,
    departmentId: req.body.departmentId,
    hours: req.body.hours
  });

  course.save().then(createdCourse => {
      res.status(201).json({
        ...createdCourse.toObject(),
        id: createdCourse._id,
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a Course failed!",
        error: error
      });
    });
}

// Updates Course
exports.updateCourse = (req, res) => {
  const course = new Course({
    _id: req.params.id,
    name: req.body.name,
    majorType: req.body.majorType,
    majorMode: req.body.majorMode,
    major: req.body.major,
    semester: req.body.semester,
    departmentId: req.body.departmentId,
    hours: req.body.hours
  });

  Course.updateOne({
      _id: req.params.id
    }, course)
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          ...course.toObject(),
          id: course._id
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Course!',
        error: error
      });
    });
}

// Get all courses
exports.getCourses = (req, res) => {
  Course.find({
      departmentId: req.params.departmentId
    })
    .select('-teachers -__v')
    .populate({
      path: 'major',
      select: '-groups -__v -majorMode -majorType'
    })
    .exec((error, fetchedCourses) => {
      if (fetchedCourses) {
        res.status(200).json(fetchedCourses);
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Courses failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: 'Courses not found!'
        });
      }
    });
}

// Get particular courses
exports.getParticularCourses = (req, res) => {

  MajorGroup.findById(req.params.groupId)
    .then(fetchedGroup => {
      if (fetchedGroup) {
        return fetchedGroup;
      } else {
        res.status(400).json({
          message: 'Major Group not found!'
        });
      }
    })
    .then(fetchedGroup => {
      Course.find({
          majorType: fetchedGroup.type,
          majorMode: fetchedGroup.mode,
          semester: fetchedGroup.semester,
          major: fetchedGroup.parentMajor
        })
        .select('-__v -teachers')
        .then((fetchedCourses) => {
          if (fetchedCourses) {
            res.status(200).json(fetchedCourses);
          } else {
            res.status(400).json({
              message: 'Courses not found!'
            });
          }
        })
        .catch(error => {
          res.status(500).json({
            message: 'Fetching Courses failed!',
            error: error
          });
        });
    })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching Major Group failed!',
        error: error
      });
    });
}

// Get course by id
exports.getCourse = (req, res) => {

  Course.findById(req.params.id)
    .populate('major')
    .exec((error, course) => {
      if (course) {
        res.status(200).json({
          ...course.toObject(),
          id: course._id
        });
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Course failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: "Course not found!"
        });
      }
    });
}

// Deletes course
exports.deleteCourse = (req, res) => {
  Course.deleteOne({
      _id: req.params.id
    })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: "Course deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting Course failed!',
        error: error
      });
    });
}
