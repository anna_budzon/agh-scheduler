const Class = require('../models/class');

// Creates Class
exports.createClass = (req, res) => {
  const newClass = new Class({
    name: req.body.name,
    majorType: req.body.majorType,
    majorMode: req.body.majorMode,
    majorId: req.body.majorId,
    semester: req.body.semester,
    teachers: req.body.teachers
  });

  newClass.save().then(createdClass => {
      res.status(201).json({
        ...createdClass.toObject(),
        id: createdClass._id,
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Creating a Class failed!",
        error: error
      });
    });
}

// Updates Class
exports.updateClass = (req, res) => {
  const updatedClass = new Class({
    _id: req.params.id,
    name: req.body.name,
    majorType: req.body.majorType,
    majorMode: req.body.majorMode,
    majorId: req.body.majorId,
    semester: req.body.semester,
    teachers: req.body.teachers
  });

  Class.updateOne({
      _id: req.params.id
    }, updatedClass)
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          ...updatedClass.toObject(),
          id: updatedClass._id
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Could not update Class!',
        error: error
      });
    });
}

// Get all classs
exports.getClasss = (req, res) => {
  Class.find({
      departmentId: req.params.id
    })
    .then(fetchedClasses => {
      res.status(200).json(fetchedClasses);
    })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching Classes failed!',
        error: error
      });
    });
}

// Get class by id
exports.getClass = (req, res) => {

  Class.findById(req.params.id)
    .populate('course')
    .populate('time')
    .populate('room')
    .populate('teacher')
    .exec((error, foundClass) => {
      if (foundClass) {
        res.status(200).json({
          ...foundClass.toObject(),
          id: foundClass._id
        });
      } else if (error) {
        res.status(500).json({
          message: 'Fetching Class failed!',
          error: error
        });
      } else {
        res.status(400).json({
          message: "Class not found!"
        });
      }
    });
}

// Deletes class
exports.deleteClass = (req, res) => {
  Class.deleteOne({
      _id: req.params.id
    })
    .then((result) => {
      if (result.n > 0) {
        res.status(200).json({
          message: "Class deleted!"
        });
      } else {
        res.status(401).json({
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting Class failed!',
        error: error
      });
    });
}
