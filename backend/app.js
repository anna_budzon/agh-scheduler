const path = require('path');
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require('mongoose');

const studentRoutes = require('./routes/student');
const teacherRoutes = require('./routes/teacher');
const departmentRoutes = require('./routes/department');
const majorRoutes = require('./routes/major');
const roomRoutes = require('./routes/room');
const courseRoutes = require('./routes/course');
const buildingRoutes = require('./routes/building');
const scheduleRoutes = require('./routes/schedule');
// const userRoutes = require('./routes/user');

const app = express();

mongoose.connect(process.env.MONGO_DB)
// mongoose.connect('mongodb://localhost/scheduler')
  .then(() => console.log("Conntected to database"))
  .catch(() => console.log("Connection failed!"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/images', express.static(path.join('backend/images')));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});

app.use('/api/student', studentRoutes);
app.use('/api/teacher', teacherRoutes);
app.use('/api/department', departmentRoutes);
app.use('/api/major', majorRoutes);
app.use('/api/course', courseRoutes);
app.use('/api/room', roomRoutes);
app.use('/api/building', buildingRoutes);
app.use('/api/schedule', scheduleRoutes);
// app.use('/api/user', userRoutes);

module.exports = app;
