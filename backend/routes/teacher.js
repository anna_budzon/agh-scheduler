const express = require('express');
const router = express.Router();

const TeacherController = require('../controllers/teacher');

router.post(
  "",
  TeacherController.createTeacher
);

router.post(
  "/multiple-create",
  TeacherController.createManyTeachers
);

router.put(
  "/multiple-update",
  TeacherController.updateManyTeachers
);

router.put(
  "/:id",
  TeacherController.updateTeacher
);

router.get("/department/:departmentId", TeacherController.getTeachers);

router.get("/:id", TeacherController.getTeacher);

router.delete(
  "/:id",
  TeacherController.deleteTeacher
);

module.exports = router;
