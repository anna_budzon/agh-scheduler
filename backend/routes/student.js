const express = require('express');
const router = express.Router();

const StudentController = require('../controllers/student');

router.post(
  "",
  StudentController.createStudent
);

router.post(
  "/multiple-create",
  StudentController.createManyStudents
);

router.put(
  "/:id",
  StudentController.updateStudent
);

router.get("/group/:majorGroupId", StudentController.getStudents);

router.get("/department/:departmentId", StudentController.getAllStudents);

router.get("/:id", StudentController.getStudent);

router.delete(
  "/:id",
  StudentController.deleteStudent
);

module.exports = router;
