const express = require('express');
const router = express.Router();

const DepartmentController = require('../controllers/department');

router.post(
  "",
  DepartmentController.createDepartment
);

router.put(
  "/:id",
  DepartmentController.updateDepartment
);

router.get("/:id", DepartmentController.getDepartment);

router.delete(
  "/:id",
  DepartmentController.deleteDepartment
);

module.exports = router;
