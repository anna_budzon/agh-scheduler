const express = require('express');
const router = express.Router();

const CourseController = require('../controllers/course');

router.post(
  "",
  CourseController.createCourse
);

router.put(
  "/:id",
  CourseController.updateCourse
);

router.get("/department/:departmentId", CourseController.getCourses);

router.get("/group/:groupId", CourseController.getParticularCourses);

router.get("/:id", CourseController.getCourse);

router.delete(
  "/:id",
  CourseController.deleteCourse
);

module.exports = router;
