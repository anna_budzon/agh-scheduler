const express = require('express');
const router = express.Router();

const ScheduleController = require('../controllers/schedule');

router.post(
  "",
  ScheduleController.initializeSchedule
);

router.put(
  "/:id/create",
  ScheduleController.createSchedule
);

router.put(
  "/:id",
  ScheduleController.updateSchedule
);

router.put(
  "/:id/preferences",
  ScheduleController.updateSchedulePreferences
);

router.get("/department/:departmentId", ScheduleController.getSchedules);

router.get("/:id", ScheduleController.getSchedule);

router.delete(
  "/:id",
  ScheduleController.deleteSchedule
);

router.post(
  "/availability",
  ScheduleController.checkAvailability
);

router.put(
  "/:id/classes",
  ScheduleController.updateClasses
);

router.get(
  "/:id/classes",
  ScheduleController.getClasses
);

router.get(
  "/:id/classes/:classId",
  ScheduleController.getClass
);

module.exports = router;
