const express = require('express');
const router = express.Router();

const MajorController = require('../controllers/major');
const MajorGroupController = require('../controllers/major-group');

router.post(
  "",
  MajorController.createMajor
);

router.put(
  "/:id",
  MajorController.updateMajor
);

router.get("/department/:departmentId", MajorController.getMajors);

router.get("/:id", MajorController.getMajor);

router.delete(
  "/:id",
  MajorController.deleteMajor
);

router.post(
  "/:id/group",
  MajorGroupController.createMajorGroup
);

router.put(
  "/:id/group/:groupId",
  MajorGroupController.updateMajorGroup
);

router.get("/:id/group", MajorGroupController.getMajorGroups);

router.get("/:id/group/:groupId", MajorGroupController.getMajorGroup);

router.delete(
  "/:id/group/:groupId",
  MajorGroupController.deleteMajorGroup
);


module.exports = router;
