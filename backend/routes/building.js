const express = require('express');
const router = express.Router();

const BuildingController = require('../controllers/building');

router.post(
  "",
  BuildingController.createBuilding
);

router.put(
  "/:id",
  BuildingController.updateBuilding
);

router.get("", BuildingController.getBuildings);

router.delete(
  "/:id",
  BuildingController.deleteBuilding
);

module.exports = router;
