const express = require('express');
const router = express.Router();

const RoomController = require('../controllers/room');

router.post(
  "",
  RoomController.createRoom
);

router.put(
  "/multiple-update",
  RoomController.updateManyRooms
);

router.put(
  "/:id",
  RoomController.updateRoom
);

router.get("/department/:departmentId", RoomController.getRooms);

router.get("/:id", RoomController.getRoom);

router.delete(
  "/:id",
  RoomController.deleteRoom
);

module.exports = router;
