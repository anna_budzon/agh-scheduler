import { Course } from './course';
import { Room } from './room';
import { Schedule } from './schedule';

export interface Teacher {
  id: string;
  firstName: string;
  lastName: string;
  title: string;
  office: Room;
  courses: Course[];
  departmentId: string;
  schedule: Schedule;
}
