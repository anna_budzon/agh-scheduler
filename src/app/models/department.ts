import { Major } from './major';

export interface Department {
  id: string;
  name: string;
  majors?: Major[];
}
