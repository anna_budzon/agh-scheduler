import { Major } from './major';
import { StudentGroups } from './student-groups';

export class Student {
  id: string;
  index: string;
  firstName: string;
  lastName: string;
  pesel: string;
  groupMajorId: string;
  major: Major;
  departmentId: string;
  classGroup: StudentGroups;
}
