import { MajorGroup } from './major-group';

// Kierunek studiów, posiadający wiele roczników studentów
export interface Major {
  id: string;
  departmentId: string;
  name: string;
  groups?: MajorGroup[]; // Groups of students with defined type of studies, semester, year, etc
  majorType?: MajorTypeEnum[]; // types of MajorTypeEnum - if master is available
  majorMode?: MajorModeEnum[]; // MajorModeEnum - if full-time and part-time is available
}

// Information if the major has
export enum MajorModeEnum {
  PART_TIME = 'PART_TIME',
  FULL_TIME = 'FULL_TIME'
}

// Information if the major includes only engineering or both engineering and master
export enum MajorTypeEnum {
  FIRST = 'FIRST',
  SECOND = 'SECOND'
}

