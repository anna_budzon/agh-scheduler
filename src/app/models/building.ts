import { Room } from './room';

// Budynek z klasami (rooms)
export interface Building {
  id: string;
  name: string;
  rooms: Room[];
}
