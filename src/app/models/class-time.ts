import { DayTypeEnum } from './day';

export interface ClassTime {
  id?: string;
  from: string;
  to: string;
  day?: DayTypeEnum;
}
