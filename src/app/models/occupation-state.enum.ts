export enum OccupationStateEnum {
  OCCUPIED = 1,
  AVAILABLE = 2,
  NONE = 3
}
