import { OccupationStateEnum } from './occupation-state.enum';
import { Conflict } from './conflict';

export interface AvailabilityResponse {
  schedule: {
      MONDAY: [{ id: string, state: OccupationStateEnum, conflicts?: Conflict[] }],
      TUESDAY: [{ id: string, state: OccupationStateEnum, conflicts?: Conflict[] }],
      WEDNESDAY: [{ id: string, state: OccupationStateEnum, conflicts?: Conflict[]  }],
      THURSDAY: [{ id: string, state: OccupationStateEnum, conflicts?: Conflict[]  }],
      FRIDAY: [{ id: string, state: OccupationStateEnum, conflicts?: Conflict[]  }],
      SATURDAY: [{ id: string, state: OccupationStateEnum, conflicts?: Conflict[]  }],
      SUNDAY: [{ id: string, state: OccupationStateEnum, conflicts?: Conflict[]  }]
    };
}
