import { CourseHours } from './course-hours';
import { Major, MajorModeEnum, MajorTypeEnum } from './major';

// Przedmiot na studiach, powiązany z konkretnym kierunkiem oraz semestrem.
// Zawiera listę nauczycieli wykładających ten przedmiot
export interface Course {
  id: string;
  name: string;
  majorType: MajorTypeEnum;
  majorMode: MajorModeEnum;
  major: Major;
  semester: string;
  departmentId: string;
  hours: CourseHours;
}
