import { MajorModeEnum, MajorTypeEnum, Major } from './major';
import { Student } from './student';
import { StudentGroups } from './student-groups';

// Rocznik studentów, o danym biezącym roku, semestrze
export interface MajorGroup {
  id: string;
  parentMajor: Major;
  students?: Student[];
  type: MajorTypeEnum;
  mode: MajorModeEnum;
  year: number;
  semester: number;
  beginYear: string;
  studentGroups: StudentGroups;
  schedule: string;
}
