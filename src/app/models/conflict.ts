export interface Conflict {
  message: string;
  conflictType: ConflictTypeEnum;
}

export enum ConflictTypeEnum {
  TEACHER_BUSY = 1,
  CLASS_OCCUPIED = 2,
  GROUP_BUSY = 3,
  UNKNOWN = 4
}
