import { Building } from './building';
import { Schedule } from './schedule';

// Pomieszczenie, w którym odbywają się zajęcia
export interface Room {
  id: string;
  number: string;
  building: Building;
  type: RoomTypeEnum;
  departmentId: string;
  limitOfPlaces: number;
  schedule: Schedule;
}

export enum RoomTypeEnum {
  PE = 'PE',
  OFFICE = 'OFFICE',
  LAB = 'LAB',
  IT = 'IT',
  REGULAR = 'REGULAR',
  LECTURE = 'LECTURE',
  SPECIAL = 'SPECIAL'
}
