export class CourseHours {
  lecture: number;
  lab: number;
  itLab: number;
  recitation: number;
  project: number;
  different: number;
}
