import { Room } from './room';
import { Course } from './course';
import { ClassTime } from './class-time';
import { Teacher } from './teacher';
import { StudentGroups } from './student-groups';

// Pojedyncze zajęcia, które odbywają się podziale godzin
export interface Class {
  id?: string;
  name?: string;
  course?: Course;
  type?: ClassTypeEnum;
  time?: ClassTime;
  room?: Room;
  regularity?: RegularityEnum;
  teacher?: Teacher;
  scheduleId: string;
  studentGroupNr?: StudentGroups;
}

export enum RegularityEnum {
  EVERY_WEEK = 'EVERY_WEEK',
  FORTNIGHTLY = 'FORTNIGHTLY'
}

export enum ClassTypeEnum {
  LAB = 'LAB', // Labolatoria
  IT = 'IT', // Komputerowe
  LECTURE = 'LECTURE',
  RECITATION = 'RECITATION', // Ćwiczenia audytoryjne,
  PROJECT = 'PROJECT',
  SPECIAL = 'SPECIAL' // e.g. PE, ang
}

