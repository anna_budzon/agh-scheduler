import { MajorGroup } from './major-group';
import { Day } from './day';
import { Major } from './major';

export interface Schedule {
  id?: string;
  days?: Day[];
  major: Major;
  majorGroup: MajorGroup;
  departmentId: string;
  teacherId?: string;
  roomId?: string;
  completed?: string;
}
