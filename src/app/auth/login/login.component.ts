import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm = new FormGroup({
    id: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$')
    ]),
    password: new FormControl('', Validators.required)
  });

  constructor(private router: Router) {}

  onSubmit() {
    if (this.loginForm.valid) {
      // this.router.navigate(['']);
    }
    this.router.navigate(['']);
  }
}
