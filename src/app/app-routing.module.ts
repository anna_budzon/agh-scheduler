import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DepartmentComponent } from './details/department/department.component';
import { GroupDetailsComponent } from './details/group-details/group-details.component';
import { MajorDetailsComponent } from './details/major-details/major-details.component';
import { ScheduleDetailsComponent } from './details/schedule-details/schedule-details.component';
import { CreateScheduleComponent } from './forms/create-schedule/create-schedule.component';
import { AllStudentsListComponent } from './lists/all-students-list/all-students-list.component';
import { CoursesListComponent } from './lists/courses-list/courses-list.component';
import { MajorsListComponent } from './lists/majors-list/majors-list.component';
import { RoomsListComponent } from './lists/rooms-list/rooms-list.component';
import { SchedulesListComponent } from './lists/schedules-list/schedules-list.component';
import { TeachersListComponent } from './lists/teachers-list/teachers-list.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { EditScheduleComponent } from './forms/edit-schedule/edit-schedule.component';
import { PreferencesScheduleComponent } from './forms/preferences-schedule/preferences-schedule.component';

const routes: Routes = [
  { path: '', component: WelcomePageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'department', component: DepartmentComponent },
  { path: 'majors', component: MajorsListComponent },
  { path: 'majors/:id', component: MajorDetailsComponent },
  { path: 'majors/:id/groups/:groupId', component: GroupDetailsComponent },
  { path: 'teachers', component: TeachersListComponent },
  { path: 'rooms', component: RoomsListComponent },
  { path: 'courses', component: CoursesListComponent },
  { path: 'students', component: AllStudentsListComponent },
  { path: 'schedules', component: SchedulesListComponent },
  { path: 'schedules/create', component: CreateScheduleComponent },
  { path: 'schedules/:id', component: ScheduleDetailsComponent },
  { path: 'schedules/:id/edit', component: EditScheduleComponent },
  { path: 'schedules/:id/preferences-edit', component: PreferencesScheduleComponent },
  { path: '**',   redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
