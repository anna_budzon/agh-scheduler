import { NgModule } from '@angular/core';
import {
  MatInputModule,
   MatCardModule,
   MatButtonModule,
   MatToolbarModule,
   MatExpansionModule,
   MatProgressSpinnerModule,
   MatPaginatorModule,
   MatDialogModule,
   MatListModule,
   MatSidenavModule,
   MatIconModule,
   MatTableModule,
   MatSelectModule,
   MatCheckboxModule,
   MatSnackBarModule,
   MatStepperModule,
   MatSortModule,
   MatTooltipModule,
   MatGridListModule,
   MatBottomSheetModule,
  } from '@angular/material';

const modules = [
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatDialogModule,
  MatListModule,
  MatSidenavModule,
  MatIconModule,
  MatTableModule,
  MatSelectModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatStepperModule,
  MatSortModule,
  MatTooltipModule,
  MatGridListModule,
  MatBottomSheetModule,
];

@NgModule({
  imports: [
    [...modules]
  ],
  exports: [
    [...modules]
  ]
})
export class AngularMaterialModule { }
