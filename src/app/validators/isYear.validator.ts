import { AbstractControl, ValidatorFn } from '@angular/forms';

export function isYearValidator(currentYear): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value !== undefined && (isNaN(control.value) ||
        control.value < (currentYear - 1000) || control.value > (currentYear + 1000)
      )) {
          return { 'invalidYear': true };
      }
      return null;
  };
}
