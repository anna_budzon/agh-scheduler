import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DeleteMajorDialogComponent } from '../../lists/majors-list/delete-major-dialog/delete-major-dialog.component';
import { Major, MajorModeEnum, MajorTypeEnum } from '../../models/major';
import { MajorService } from '../../services/major.service';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';

@Component({
  selector: 'app-major-details',
  templateUrl: './major-details.component.html',
  styleUrls: ['./major-details.component.scss']
})
export class MajorDetailsComponent implements OnInit, OnDestroy {
  major: Major;
  editMajorForm: FormGroup;
  viewMode = true;
  majorType: string;
  majorMode: string;
  types = [
    { value: MajorTypeEnum.FIRST, viewValue: 'I stopień' },
    { value: MajorTypeEnum.SECOND, viewValue: 'II stopień' }
  ];
  modes = [
    { value: MajorModeEnum.FULL_TIME, viewValue: 'Stacjonarne' },
    { value: MajorModeEnum.PART_TIME, viewValue: 'Niestacjonarne' }
  ];
  routeParamsSub: Subscription;
  majorSub: Subscription;
  updateMajorSub: Subscription;
  deleteMajorSub: Subscription;

  constructor(
    private majorService: MajorService,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    let id: string;
    this.routeParamsSub = this.route.params
    .subscribe(params => {
      id = params['id'];
    });

    this.editMajorForm = new FormGroup({
      name: new FormControl('')
    });

    this.majorSub = this.majorService.getMajor(id)
      .subscribe((major: Major) => {
        this.major = major;
        if (this.major) {
          this.editMajorForm.setValue({ name: this.major.name });
          this.createTypeModeStrings();
        }
      });
  }

  createTypeModeStrings() {
    if (this.major.majorType && this.major.majorType.length) {
      const first = this.major.majorType.some(m => m === MajorTypeEnum.FIRST) ? 'I stopień' : '';
      const second = this.major.majorType.some(m => m === MajorTypeEnum.SECOND) ? 'II stopień' : '';
      this.majorType = first && second ? `${first}, ${second}` : `${first}${second}`;
    } else {
      this.majorType = 'BRAK';
    }

    if (this.major.majorMode && this.major.majorMode.length) {
      const full = this.major.majorMode.some(m => m === MajorModeEnum.FULL_TIME) ? 'Stacjonarne' : '';
      const part = this.major.majorMode.some(m => m === MajorModeEnum.PART_TIME) ? 'Niestacjonarne' : '';
      this.majorMode = full && part ? `${full}, ${part}` : `${full}${part}`;
    } else {
      this.majorMode = 'BRAK';
    }
  }

  checkMajorType(type: { value: MajorTypeEnum }) {
    return this.major ?
      this.major.majorType.some(t =>  t === type.value) : false;
  }

  updateMajorTypes(type: { value: MajorTypeEnum }) {
    if (!this.major) { return; }

    const typeIdx = this.major.majorType.findIndex(t => t === type.value);
    typeIdx !== -1 ? this.major.majorType.splice(typeIdx, 1) : this.major.majorType.push(type.value);
    this.createTypeModeStrings();
  }

  checkMajorMode(mode:  { value: MajorModeEnum }) {
    return this.major ?
      this.major.majorMode.some(m => m === mode.value) : false;
  }

  updateMajorModes(mode: { value: MajorModeEnum }) {
    const modeIdx = this.major.majorMode.findIndex(m => m === mode.value);
    modeIdx !== -1 ? this.major.majorMode.splice(modeIdx, 1) : this.major.majorMode.push(mode.value);
    this.createTypeModeStrings();
  }

  updateMajor() {
    this.updateMajorSub = this.majorService.updateMajor(this.major)
      .subscribe((updatedMajor: Major) => {
        this.major = { ...updatedMajor };
        this.viewMode = true;
        this.snackBar.openFromComponent(InformSnackBarComponent, {
          data: { message: `Kierunek ${updatedMajor} został zaktualizowany.` },
          duration: 2000,
          verticalPosition: 'bottom',
          horizontalPosition: 'left'
        });
      });
  }

  deleteMajor(deletedMajor: Major) {
    const dialogRef = this.dialog.open(DeleteMajorDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.deleteMajorSub = this.majorService.deleteMajor(deletedMajor.id)
            .subscribe(() => {
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: `Kierunek  ${deletedMajor.name} został usunięty.` },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
              this.router.navigate(['majors']);
            });
        }
    });
  }

  goBack() {
    if (this.viewMode) {
      this.router.navigate([`majors`]);
    } else {
      this.viewMode = true;
      this.editMajorForm.setValue({ name: this.major.name });
    }
  }

  ngOnDestroy() {
    if (this.routeParamsSub) { this.routeParamsSub.unsubscribe(); }
    if (this.majorSub) { this.majorSub.unsubscribe(); }
    if (this.updateMajorSub) { this.updateMajorSub.unsubscribe(); }
    if (this.deleteMajorSub) { this.deleteMajorSub.unsubscribe(); }
  }
}
