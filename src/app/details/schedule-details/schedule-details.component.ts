import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Class, ClassTypeEnum } from '../../models/class';
import { ClassTime } from '../../models/class-time';
import { Room } from '../../models/room';
import { Schedule } from '../../models/schedule';
import { Teacher } from '../../models/teacher';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { CommonService } from '../../services/common.service';
import { ScheduleService } from '../../services/schedule.service';
import { TeacherService } from '../../services/teacher.service';
import { DeleteScheduleDialogComponent } from './delete-schedule-dialog/delete-schedule-dialog.component';

@Component({
  selector: 'app-schedule-details',
  templateUrl: './schedule-details.component.html',
  styleUrls: ['./schedule-details.component.scss']
})
export class ScheduleDetailsComponent implements OnInit, OnDestroy {

  schedule: Schedule;
  nameInfo = '';
  classTime = [
    { id: 0, from: '8:00', to: '9:30' },
    { id: 1, from: '9:45', to: '11:15' },
    { id: 2, from: '11:30', to: '13:00' },
    { id: 3, from: '13:15', to: '14:45' },
    { id: 4, from: '15:00', to: '16:30' },
    { id: 5, from: '16:45', to: '18:15' },
    { id: 6, from: '18:30', to: '20:00' }
  ];
  routeSub: Subscription;
  deleteSub: Subscription;
  scheduleSub: Subscription;
  teacherSub: Subscription;
  roomSub: Subscription;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private scheduleService: ScheduleService,
    private teacherService: TeacherService,
    private commonService: CommonService,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    let scheduleId = '';
    this.routeSub = this.route.params
      .subscribe((params) => {
        scheduleId = params['id'];
      });
    this.scheduleSub = this.scheduleService.getSchedule(scheduleId)
      .subscribe((schedule) => {
        this.schedule = schedule;
        this.getNameInfo();
      });
  }

  createSchedule() {
    this.router.navigate(['/schedules/create']);
  }

  getScheduledClasses(classes: Class[], elem: ClassTime) {
    return classes.filter(cls => cls.time.id === elem.id);
  }

  getNameInfo() {
    if (this.schedule.teacherId) {
      this.teacherSub = this.teacherService.getTeacher(this.schedule.teacherId)
        .subscribe((teacher) => {
          this.nameInfo = `${teacher.title} ${teacher.firstName} ${teacher.lastName}`;
        });
    } else if (this.schedule.roomId) {
      this.roomSub = this.commonService.getRoom(this.schedule.roomId)
        .subscribe((room) => {
          this.nameInfo = `Budynek ${room.building.name}, sala ${room.number}`;
        });
    } else {
      this.nameInfo = `${this.schedule.major.name},
        rocznik ${this.schedule.majorGroup.beginYear}/${this.schedule.majorGroup.beginYear + 1},
        semestr ${this.schedule.majorGroup.semester}`;
    }
  }

  getClassDetails(currentCls: Class) {
    if (currentCls.name) {
    return `${currentCls.name} : G${this.getStudentGroup(currentCls.type, currentCls.studentGroupNr)},
      ${this.getRoom(currentCls.room)}, ${this.getTeacher(currentCls.teacher)}`;
    } else {
      return 'TERMIN ZAJĘTY';
    }
  }

  getStudentGroup(type: ClassTypeEnum, studentGroupNr) {
    let group = '';

    switch (type) {
      case ClassTypeEnum.IT:
      case ClassTypeEnum.LAB:
        group = `ĆL-${studentGroupNr.labGroups}`;
        break;
      case ClassTypeEnum.LECTURE:
        group = `W`;
        break;
      case ClassTypeEnum.PROJECT:
        group = `P-${studentGroupNr.regularGroups}`;
        break;
      case ClassTypeEnum.RECITATION:
        group = `A-${studentGroupNr.regularGroups}`;
        break;
      default:
        group = 'N/A';
    }
    return group;
  }

  getTeacher(teacher: Teacher) {
    return `${teacher.title} ${teacher.lastName}`;
  }

  getRoom(room: Room) {
    return `${room.building.name}-${room.number}`;
  }

  editSchedule() {
    const id = this.schedule.id ? this.schedule.id : (this.schedule as any)._id;

    if (!this.schedule.teacherId && !this.schedule.roomId) {
      this.router.navigate([`/schedules/${id}/edit`]);
    } else {
      this.router.navigate([`/schedules/${id}/preferences-edit`]);
    }
  }

  deleteSchedule() {
    const id = this.schedule.id ? this.schedule.id : (this.schedule as any)._id;

    const dialogRef = this.dialog.open(DeleteScheduleDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.deleteSub = this.scheduleService.deleteSchedule(id)
            .subscribe(() => {
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: 'Plan zajęć został usunięty.' },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
              this.router.navigate(['/schedules']);
            });
        }
      });
  }

  goBack() {
   this.location.back();
  }

  ngOnDestroy() {
    if (this.routeSub) { this.routeSub.unsubscribe(); }
    if (this.deleteSub) { this.deleteSub.unsubscribe(); }
    if (this.scheduleSub) { this.scheduleSub.unsubscribe(); }
    if (this.roomSub) { this.roomSub.unsubscribe(); }
    if (this.teacherSub) { this.teacherSub.unsubscribe(); }
  }
}
