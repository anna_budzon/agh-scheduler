import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Department } from '../../models/department';
import { DepartmentService } from '../../services/department.service';
import { Globals } from '../../../environments/globals';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent implements OnInit, OnDestroy {
  department: Department;
  departmentSub: Subscription;

  constructor(
    private departmentService: DepartmentService,
    public globals: Globals
  ) {}

  ngOnInit() {
    this.department = {
      id: '5c6431f49132f3266097476d',
      name: 'Wydział Inżynierii Metali i Informatyki Przemysłowej',
      majors: []
    };

    this.departmentSub = this.departmentService.setDepartment(this.department)
      .subscribe((department: Department) => {
        console.log(department)
        this.globals.department.id = department.id;
      });    
  }

  ngOnDestroy() {
    if (this.departmentSub) { this.departmentSub.unsubscribe(); }
  }
}
