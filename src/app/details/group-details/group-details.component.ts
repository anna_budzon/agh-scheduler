import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DeleteMajorDialogComponent } from '../../lists/majors-list/delete-major-dialog/delete-major-dialog.component';
import { MajorModeEnum, MajorTypeEnum } from '../../models/major';
import { MajorGroup } from '../../models/major-group';
import { MajorService } from '../../services/major.service';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { isYearValidator } from '../../validators/isYear.validator';
import { ErrorSnackBarComponent } from '../../pop-ups/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.scss']
})
export class GroupDetailsComponent implements OnInit, OnDestroy {

  group: MajorGroup;
  majorId: string;
  groupId: string;
  editMajorGroupForm: FormGroup;
  types = [
    { value: MajorTypeEnum.FIRST, viewValue: 'I stopień' },
    { value: MajorTypeEnum.SECOND, viewValue: 'II stopień' }
  ];
  modes = [
    { value: MajorModeEnum.FULL_TIME, viewValue: 'Stacjonarne' },
    { value: MajorModeEnum.PART_TIME, viewValue: 'Niestacjonarne' }
  ];
  years = [];
  semesters = [];
  viewMode = true;
  deleteGroupSub: Subscription;
  updateGroupSub: Subscription;
  routeParamsSub: Subscription;
  getGroupSub: Subscription;

  constructor(
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private majorService: MajorService
  ) { }

  ngOnInit() {
      this.routeParamsSub = this.route.params
      .subscribe(params => {
        this.majorId = params['id'];
        this.groupId = params['groupId'];
      });

      this.getGroupSub = this.majorService.getMajorGroup(this.majorId, this.groupId)
        .subscribe((group: MajorGroup) => {
          this.group = group;
          this.defineSelectOptions();
          this.setFormsValues();
        });

      this.editMajorGroupForm = new FormGroup({
        type: new FormControl('', [
          Validators.required
        ]),
        mode: new FormControl('',  [
          Validators.required
        ]),
        year: new FormControl('',  [
          Validators.required
        ]),
        semester: new FormControl('',  [
          Validators.required
        ]),
        beginYear: new FormControl('',  [
          Validators.required,
          isYearValidator((new Date).getFullYear())
        ]),
      });
    }

  defineSelectOptions() {
    if (this.group.mode === MajorModeEnum.FULL_TIME) {
      this.years = [1, 2, 3, 4, 5];
      this.semesters = this.group.type === MajorTypeEnum.FIRST ?
        [1, 2, 3, 4, 5, 6, 7] :
        [1, 2, 3];
    } else {
      this.years = [1, 2, 3, 4, 5, 6];
      this.semesters = this.group.type === MajorTypeEnum.FIRST ?
        [1, 2, 3, 4, 5, 6, 7, 8] :
        [1, 2, 3, 4];
    }
  }

  setFormsValues() {
    this.editMajorGroupForm.setValue({
      type: this.group.type,
      mode: this.group.mode,
      year: this.group.year,
      semester: this.group.semester,
      beginYear: this.group.beginYear
    });
  }

  updateMajorGroup() {
    if (this.editMajorGroupForm.valid) {
      this.updateGroupSub = this.majorService
        .updateMajorGroup(this.majorId, this.groupId, this.editMajorGroupForm.value)
          .subscribe((updatedMajorGroup: MajorGroup) => {
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: 'Grupa została zaktualizowana.' },
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'left'
            });
            this.group = { ...this.group, ...updatedMajorGroup };
            this.viewMode = true;
          });


    }
  }

  deleteMajorGroup() {
    const dialogRef = this.dialog.open(DeleteMajorDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.deleteGroupSub = this.majorService.deleteMajorGroup(this.majorId, this.groupId)
            .subscribe(() => {
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: 'Grupa została usunięta.' },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
              this.router.navigate([`majors/${this.majorId}`]);
            });
        }
      });
  }

  displaySchedule(group: MajorGroup) {
    if (group.schedule) {
      this.router.navigate([`schedules/${group.schedule}`]);
    } else {
      this.snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: { message: 'Grupa nie ma jeszcze przygotowanego planu zajęć!' },
        duration: 2000,
        verticalPosition: 'bottom',
        horizontalPosition: 'left'
      });
    }
  }

  goBack() {
    if (this.viewMode) {
      this.router.navigate([`majors/${this.majorId}`]);
    } else {
      this.setFormsValues();
      this.viewMode = true;
    }
  }

  ngOnDestroy() {
    if (this.routeParamsSub) { this.routeParamsSub.unsubscribe(); }
    if (this.getGroupSub) { this.getGroupSub.unsubscribe(); }
    if (this.updateGroupSub) { this.updateGroupSub.unsubscribe(); }
    if (this.deleteGroupSub) { this.deleteGroupSub.unsubscribe(); }
  }
}
