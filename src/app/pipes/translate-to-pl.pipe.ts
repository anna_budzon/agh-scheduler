import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'translateToPl'
})
export class TranslateToPlPipe implements PipeTransform {

  transform(value: string): string {
    switch (value) {
      case 'MONDAY':
        return 'Poniedziałek'.toUpperCase();
      case 'TUESDAY':
        return 'Wtorek'.toUpperCase();
      case 'WEDNESDAY':
        return 'ŚRODA';
      case 'THURSDAY':
        return 'Czwartek'.toUpperCase();
      case 'FRIDAY':
        return 'Piątek'.toUpperCase();
      case 'SATURDAY':
        return 'Sobota'.toUpperCase();
      case 'SUNDAY':
        return 'Niedziela'.toUpperCase();
      default:
        return 'None';
    }
  }
}
