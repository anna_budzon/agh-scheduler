import { Directive, Input, ElementRef } from '@angular/core';
import { OccupationStateEnum } from '../models/occupation-state.enum';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private el: ElementRef) {}

  @Input('appHighlight') set state(occupationState: OccupationStateEnum) {
    switch (+occupationState) {
      case OccupationStateEnum.OCCUPIED:
        this.el.nativeElement.style.cssText = `-moz-box-shadow: inset 0 0 10px #ff1a1a;
                                               -webkit-box-shadow: inset 0 0 10px #ff1a1a;
                                               box-shadow:inset 0 0 10px #ff1a1a;`;
        break;
      case OccupationStateEnum.AVAILABLE:
        this.el.nativeElement.style.cssText = `-moz-box-shadow: inset 0 0 10px #00cc00;
                                               -webkit-box-shadow: inset 0 0 10px #00cc00;
                                               box-shadow:inset 0 0 10px #00cc00;`;
        break;
      default:
      this.el.nativeElement.style.cssText = '';
    }
  }
}
