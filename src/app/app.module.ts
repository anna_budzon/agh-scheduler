import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { AppComponent } from './app.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './auth/login/login.component';
import { StudentsListComponent } from './lists/students-list/students-list.component';
import { MainSideNavComponent } from './main-side-nav/main-side-nav.component';
import { MainContentWrapperComponent } from './main-content-wrapper/main-content-wrapper.component';
import { AddMajorDialogComponent } from './lists/majors-list/add-major-dialog/add-major-dialog.component';
import { DeleteMajorDialogComponent } from './lists/majors-list/delete-major-dialog/delete-major-dialog.component';
import { MajorsListComponent } from './lists/majors-list/majors-list.component';
import { MajorDetailsComponent } from './details/major-details/major-details.component';
import { GroupDetailsComponent } from './details/group-details/group-details.component';
import { HttpClientModule } from '@angular/common/http';
import { DeleteStudentDialogComponent } from './lists/students-list/delete-student-dialog/delete-student-dialog.component';
import { AddStudentDialogComponent } from './lists/students-list/add-student-dialog/add-student-dialog.component';
import { TeachersListComponent } from './lists/teachers-list/teachers-list.component';
import { AddTeacherDialogComponent } from './lists/teachers-list/add-teacher-dialog/add-teacher-dialog.component';
import { DeleteTeacherDialogComponent } from './lists/teachers-list/delete-teacher-dialog/delete-teacher-dialog.component';
import { AddMajorGroupDialogComponent } from './lists/major-groups-list/add-major-group-dialog/add-major-group-dialog.component';
import { MajorGroupsListComponent } from './lists/major-groups-list/major-groups-list.component';
import { RoomsListComponent } from './lists/rooms-list/rooms-list.component';
import { DepartmentComponent } from './details/department/department.component';
import { CoursesListComponent } from './lists/courses-list/courses-list.component';
import { AddCourseDialogComponent } from './lists/courses-list/add-course-dialog/add-course-dialog.component';
import { DeleteCourseDialogComponent } from './lists/courses-list/delete-course-dialog/delete-course-dialog.component';
import { AllStudentsListComponent } from './lists/all-students-list/all-students-list.component';
import { SchedulesListComponent } from './lists/schedules-list/schedules-list.component';
import { CreateScheduleComponent } from './forms/create-schedule/create-schedule.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ErrorSnackBarComponent } from './pop-ups/error-snack-bar/error-snack-bar.component';
import { InformSnackBarComponent } from './pop-ups/inform-snack-bar/inform-snack-bar.component';
import { CreateClassesComponent } from './forms/create-schedule/create-classes/create-classes.component';
import { ScheduleClassesComponent } from './forms/create-schedule/schedule-classes/schedule-classes.component';
import { OptionMenuSheetComponent } from './pop-ups/option-menu-sheet/option-menu-sheet.component';
import { TranslateToPlPipe } from './pipes/translate-to-pl.pipe';
import { HighlightDirective } from './directives/highlight.directive';
import { Globals } from '../environments/globals';
import { ConfirmationDialogComponent } from './forms/create-schedule/confirmation-dialog/confirmation-dialog.component';
import { ScheduleDetailsComponent } from './details/schedule-details/schedule-details.component';
import { ScheduleDontExistDialogComponent } from './pop-ups/schedule-dont-exist-dialog/schedule-dont-exist-dialog.component';
import { EditScheduleComponent } from './forms/edit-schedule/edit-schedule.component';
import { DeleteScheduleDialogComponent } from './details/schedule-details/delete-schedule-dialog/delete-schedule-dialog.component';
import { PreferencesScheduleComponent } from './forms/preferences-schedule/preferences-schedule.component';
import { PreferencesMenuSheetComponent } from './pop-ups/preferences-menu-sheet/preferences-menu-sheet.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomePageComponent,
    HeaderComponent,
    LoginComponent,
    MainSideNavComponent,
    StudentsListComponent,
    DepartmentComponent,
    MainContentWrapperComponent,
    AddMajorDialogComponent,
    DeleteMajorDialogComponent,
    MajorsListComponent,
    MajorDetailsComponent,
    GroupDetailsComponent,
    AddStudentDialogComponent,
    DeleteStudentDialogComponent,
    AddMajorGroupDialogComponent,
    MajorGroupsListComponent,
    TeachersListComponent,
    AddTeacherDialogComponent,
    DeleteTeacherDialogComponent,
    RoomsListComponent,
    CoursesListComponent,
    AddCourseDialogComponent,
    DeleteCourseDialogComponent,
    AllStudentsListComponent,
    SchedulesListComponent,
    CreateScheduleComponent,
    ErrorSnackBarComponent,
    InformSnackBarComponent,
    CreateClassesComponent,
    ScheduleClassesComponent,
    OptionMenuSheetComponent,
    TranslateToPlPipe,
    HighlightDirective,
    ConfirmationDialogComponent,
    ScheduleDetailsComponent,
    ScheduleDontExistDialogComponent,
    EditScheduleComponent,
    DeleteScheduleDialogComponent,
    PreferencesScheduleComponent,
    PreferencesMenuSheetComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    AngularMaterialModule,
    DragDropModule
  ],
  providers: [
    TranslateToPlPipe,
    Globals
  ],
  entryComponents: [
    AddMajorDialogComponent,
    DeleteMajorDialogComponent,
    AddStudentDialogComponent,
    DeleteStudentDialogComponent,
    AddMajorGroupDialogComponent,
    AddTeacherDialogComponent,
    DeleteTeacherDialogComponent,
    AddCourseDialogComponent,
    DeleteCourseDialogComponent,
    ErrorSnackBarComponent,
    InformSnackBarComponent,
    OptionMenuSheetComponent,
    ConfirmationDialogComponent,
    ScheduleDontExistDialogComponent,
    DeleteScheduleDialogComponent,
    PreferencesMenuSheetComponent
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
