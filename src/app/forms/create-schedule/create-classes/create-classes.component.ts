import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';
import { Globals } from '../../../../environments/globals';
import { Class, ClassTypeEnum, RegularityEnum } from '../../../models/class';
import { Course } from '../../../models/course';
import { CourseHours } from '../../../models/course-hours';
import { MajorModeEnum, MajorTypeEnum } from '../../../models/major';
import { MajorGroup } from '../../../models/major-group';
import { Room, RoomTypeEnum } from '../../../models/room';
import { StudentGroups } from '../../../models/student-groups';
import { Teacher } from '../../../models/teacher';
import { CommonService } from '../../../services/common.service';
import { TeacherService } from '../../../services/teacher.service';

@Component({
  selector: 'app-create-classes',
  templateUrl: './create-classes.component.html',
  styleUrls: ['./create-classes.component.scss']
})
export class CreateClassesComponent implements OnInit, OnDestroy {
  @Output() formChanges = new EventEmitter<boolean>();
  @Output() classesChanges = new EventEmitter<Class[]>();
  @Input() scheduleId: string;
  @Input() majorName = '';
  @Input() majorGroup: MajorGroup;
  @Input() set courses(listOfCourses: Course[]) {
    if (this.scheduleId && listOfCourses && this.majorGroup) {
      this.classes = [];
      let k = 0;
      listOfCourses.forEach((course: Course) => {
        const typesOfClasses = this.getClassTypes(course.hours);

        typesOfClasses.forEach((type: ClassTypeEnum) => {
          const regularity = this.getClassRegularity(course.hours, type);
          let count = 1;

          if (type === ClassTypeEnum.LAB || type === ClassTypeEnum.IT) {
            count = this.majorGroup.studentGroups.labGroups;
          } else if (type === ClassTypeEnum.LECTURE) {
            count = 1;
          } else {
            count = this.majorGroup.studentGroups.regularGroups;
          }

          for (let i = 0; i < count; i++) {
            const newClass: Class = {
              id: k.toString(),
              name: course.name,
              course: course,
              type: type,
              regularity: regularity,
              scheduleId: this.scheduleId,
              studentGroupNr: type === ClassTypeEnum.LAB || type === ClassTypeEnum.IT ? { labGroups: i + 1 } : { regularGroups: i + 1 }
            };
            k++;
            this.classes.push(newClass);
          }
        });
      });
      this.refresh();
    }
  }

  classesForm = new FormGroup({
    classes: this.formBuilder.array([])
  });

  classes: Class[] = [];
  teachers: Teacher[] = [];
  rooms: Room[] = [];
  dataSource = new MatTableDataSource<Class>([]);
  getTeachersSub: Subscription;
  getRoomsSub: Subscription;

  constructor(
    private teacherService: TeacherService,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private globals: Globals
  ) {}

  ngOnInit() {
    this.getTeachersSub = this.teacherService.getTeachers(this.globals.department.id)
      .subscribe((teachers: Teacher[]) => {
        this.teachers = teachers.sort((a, b) => {
          if (a.lastName < b.lastName) {
            return -1;
          }
          if (a.lastName > b.lastName) {
            return 1;
          }
          return 0;
        });
      });
    this.getRoomsSub = this.commonService.getRooms(this.globals.department.id)
      .subscribe((rooms: Room[]) => {
        this.rooms = rooms;
      });

    this.classesForm.get('classes').valueChanges.subscribe(classes => {
      const isValid = classes &&
        classes.length &&
        (this.classesForm.get('classes') as FormArray).controls.every(ctrl => ctrl.valid);

      this.formChanges.emit(isValid);
      if (isValid) {
        this.classes.forEach((cls, i) => {
          if (this.classes.indexOf(cls) !== -1) {
            cls = {
              ...cls,
              room: classes[this.classes.indexOf(cls)].room,
              teacher: classes[this.classes.indexOf(cls)].teacher
            };
            this.classes.splice(i, 1, cls);
          } else {
            cls = null;
          }
        });
        this.classesChanges.emit(this.classes);
      }
    });
  }

  getClassRegularity(classHours: CourseHours, type: ClassTypeEnum) {
    let regularity: RegularityEnum;
    let typeToString = type.toLowerCase();
    if (typeToString === 'special') {
      typeToString = 'different';
    }

    classHours[typeToString] > 15 ? (regularity = RegularityEnum.EVERY_WEEK) : (regularity = RegularityEnum.FORTNIGHTLY);

    return regularity;
  }

  getClassTypes(classHours: CourseHours) {
    if (!classHours) {
      return [];
    }

    const types: ClassTypeEnum[] = [];

    Object.keys(classHours).forEach(type => {
      if (classHours[type] > 0) {
        switch (type) {
          case 'lecture':
            types.push(ClassTypeEnum.LECTURE);
            break;
          case 'lab':
            types.push(ClassTypeEnum.LAB);
            break;
          case 'itLab':
            types.push(ClassTypeEnum.IT);
            break;
          case 'recitation':
            types.push(ClassTypeEnum.RECITATION);
            break;
          case 'project':
            types.push(ClassTypeEnum.PROJECT);
            break;
          default:
            types.push(ClassTypeEnum.SPECIAL);
            break;
        }
      }
    });

    return types;
  }

  getCourseInfo(course: Course) {
    const type = course.majorType === MajorTypeEnum.FIRST ? 'I stopień' : 'II stopień';
    const mode = course.majorMode === MajorModeEnum.FULL_TIME ? 'stacjonarne' : 'niestacjonarne';
    return `${this.majorName}, ${type}, ${mode}, semestr ${course.semester}`;
  }

  getTypeInfo(type: ClassTypeEnum) {
    let typeInfo = '';

    switch (type) {
      case ClassTypeEnum.LECTURE:
        typeInfo = 'Wykład';
        break;
      case ClassTypeEnum.LAB:
        typeInfo = 'Labolatorium';
        break;
      case ClassTypeEnum.IT:
        typeInfo = 'Ćwiczenia komputerowe';
        break;
      case ClassTypeEnum.RECITATION:
        typeInfo = 'Ćwiczenia audytoryjne';
        break;
      case ClassTypeEnum.PROJECT:
        typeInfo = 'Ćwiczenia projektowe';
        break;
      default:
        typeInfo = 'Inne';
        break;
    }

    return typeInfo;
  }

  getTeacherInfo(teacher: Teacher) {
    return `${teacher.lastName} ${teacher.firstName} (${teacher.title})`;
  }

  getStudentGroup(group: StudentGroups, type: ClassTypeEnum) {
    switch (type) {
      case ClassTypeEnum.IT:
      case ClassTypeEnum.LAB:
        return `ĆL-${group.labGroups}`;
      case ClassTypeEnum.LECTURE:
        return `W`;
      case ClassTypeEnum.PROJECT:
        return `P-${group.regularGroups}`;
      case ClassTypeEnum.RECITATION:
        return `A-${group.regularGroups}`;
      default:
        return 'SPECIAL';
    }
  }

  getRoomInfo(room: Room) {
    return `${room.building.name} ${room.number}. Ilość osób: ${room.limitOfPlaces}`;
  }

  filterRooms(cl: Class) {
    switch (cl.type) {
      case ClassTypeEnum.LECTURE:
        return this.rooms.filter(room => room.type === RoomTypeEnum.LECTURE);
      case ClassTypeEnum.LAB:
        return this.rooms.filter(room => room.type === RoomTypeEnum.LAB);
      case ClassTypeEnum.IT:
        return this.rooms.filter(room => room.type === RoomTypeEnum.IT);
      case ClassTypeEnum.PROJECT:
      case ClassTypeEnum.RECITATION:
        return this.rooms.filter(room => room.type === RoomTypeEnum.REGULAR);

      case ClassTypeEnum.SPECIAL:
        return this.rooms.filter(room => room.type === RoomTypeEnum.SPECIAL);
    }
  }

  getColumns() {
    return ['name', 'course', 'type', 'group', 'room', 'teacher', 'regularity'];
  }

  refresh() {
    this.dataSource.data = this.classes;
    const classCtrl = this.classesForm.get('classes') as FormArray;
    while (classCtrl.length !== 0) {
      classCtrl.removeAt(0);
    }
    this.dataSource.data.forEach(cl => {
      classCtrl.push(this.setClassesFormArray(cl));
    });
  }

  setClassesFormArray(cl: Class) {
    return this.formBuilder.group({
      room: [cl.room, Validators.required],
      teacher: [cl.teacher, Validators.required]
    });
  }

  ngOnDestroy() {
    if (this.getTeachersSub) { this.getTeachersSub.unsubscribe(); }
    if (this.getRoomsSub) { this.getRoomsSub.unsubscribe(); }
  }
}
