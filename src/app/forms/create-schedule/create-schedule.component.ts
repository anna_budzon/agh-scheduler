import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from '../../../environments/globals';
import { Class } from '../../models/class';
import { Course } from '../../models/course';
import { Day } from '../../models/day';
import { Major, MajorModeEnum, MajorTypeEnum } from '../../models/major';
import { MajorGroup } from '../../models/major-group';
import { Schedule } from '../../models/schedule';
import { ErrorSnackBarComponent } from '../../pop-ups/error-snack-bar/error-snack-bar.component';
import { CourseService } from '../../services/course.service';
import { MajorService } from '../../services/major.service';
import { ScheduleService } from '../../services/schedule.service';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-create-schedule',
  templateUrl: './create-schedule.component.html',
  styleUrls: ['./create-schedule.component.scss']
})
export class CreateScheduleComponent implements OnInit, OnDestroy {
  majorForm = new FormGroup({
    major: new FormControl('', [Validators.required])
  });
  groupForm = new FormGroup({
    group: new FormControl('', [Validators.required])
  });
  coursesForm = new FormGroup({
    courses: new FormControl('', [Validators.required])
  });

  majors: Major[];
  majorGroups: MajorGroup[];
  allCourses: Course[];
  chosenCourses: Course[] = [];
  chosenMajorGroup: MajorGroup;
  schedule: Schedule;
  completedSchedule: Schedule;
  classes: Class[];
  creationCompleted = false;
  ifClassFormValid = false;
  getMajorsSub: Subscription;
  getGroupsSub: Subscription;
  getCoursesSub: Subscription;
  addScheduleSub: Subscription;

  constructor(
    private majorService: MajorService,
    private courseService: CourseService,
    private scheduleService: ScheduleService,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private globals: Globals
  ) {}

  ngOnInit() {
    this.getMajorsSub = this.majorService.getMajors(this.globals.department.id)
      .subscribe((majors: Major[]) => {
        this.majors = majors;
      });
  }

  fetchGroups() {
    this.getGroupsSub = this.majorService.getMajorGroups(this.majorForm.value.major.id)
      .pipe(
        map(groups => {
          return groups.filter(g => (!g.schedule || !g.schedule.completed));
        })
      )
      .subscribe((groups: MajorGroup[]) => {
        this.majorGroups = groups;
      });
  }

  fetchCourses() {
    this.chosenMajorGroup = this.groupForm.value.group;

    this.addScheduleSub = this.scheduleService.initializeSchedule({
      days: this.getDays(),
      departmentId: this.globals.department.id,
      majorGroup: this.groupForm.value.group.id,
      major: this.majorForm.value.major.id
    })
      .subscribe((schedule: Schedule) => {
        this.schedule = schedule;
      });

    this.getCoursesSub = this.courseService.getParticularCourses(this.groupForm.value.group.id)
      .subscribe((courses: Course[]) => {
        this.allCourses = courses;
      });
  }

  getDays() {
    const dayType = [
      'MONDAY',
      'TUESDAY',
      'WEDNESDAY',
      'THURSDAY',
      'FRIDAY'
    ];

    return dayType.map(type => {
      return ({ dayType: type, classes: [] } as Day);
    });
  }

  getCoursesNames(courses: Course[]) {
    return courses ? courses.map(course => course.name) : '';
  }

  getGroupInfo(group: MajorGroup) {
    const type = group.type === MajorTypeEnum.FIRST ? 'I stopień' : 'II stopień';
    const mode = group.mode === MajorModeEnum.FULL_TIME ? 'stacjonarne' : 'niestacjonarne';
    return `Rocznik ${group.beginYear}/${+group.beginYear + 1}, ${type}, ${mode}, semestr ${group.semester}, rok ${group.year}`;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
      if (event.previousContainer.id === 'cdk-drop-list-1') {
        this.coursesForm.setValue({ courses : event.previousContainer.data });
      } else if (event.container.id === 'cdk-drop-list-1') {
        this.coursesForm.setValue({ courses : event.container.data });
      } else {
        console.log('here', event);
      }
    }
  }

  moveEveryCourse() {
    if (this.allCourses.length) {
      this.coursesForm.setValue({ courses: this.allCourses.concat(this.chosenCourses) });
      this.chosenCourses = this.allCourses.concat(this.chosenCourses);
      this.allCourses = [];
    } else {
      this.allCourses = this.allCourses.concat(this.chosenCourses);
      this.coursesForm.setValue({ courses: [] });
      this.chosenCourses = [];
    }
  }

  isCourseFormValid() {
    if (this.coursesForm.invalid) {
      this.snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: { message: 'Wymagane jest dodanie przedmiotów!' },
        duration: 2000,
        verticalPosition: 'bottom',
        horizontalPosition: 'left'
      });
    } else {
      this.chosenCourses = [ ...this.chosenCourses ];
    }
  }

  isClassFormValid(event) {
    this.ifClassFormValid = event;
  }

  goBack() {
    this.router.navigate(['']);
  }

  setClasses(classes: Class[]) {
    this.classes = classes;
  }

  setSchedule(schedule: Schedule) {
    if (schedule) {
      this.completedSchedule = { ...schedule };
      this.creationCompleted = true;
    } else {
      this.completedSchedule = null;
      this.creationCompleted = false;
    }
  }

  clean() {
    this.classes = [];
    this.allCourses = this.allCourses.concat(this.chosenCourses);
    this.coursesForm.setValue({ courses: [] });
    this.chosenCourses = [];
    this.ifClassFormValid = false;
    this.completedSchedule = null;
  }

  saveSchedule() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        major: this.majorForm.value.major.name,
        group: this.chosenMajorGroup
      }
    });
    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.scheduleService.createSchedule(this.completedSchedule)
            .subscribe(() => {
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: 'Plan zajęć został pomyślnie dodany!' },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
            });
            this.router.navigate(['schedules']);
        }
      });
  }

  ngOnDestroy() {
    if (this.getGroupsSub) { this.getGroupsSub.unsubscribe(); }
    if (this.getMajorsSub) { this.getMajorsSub.unsubscribe(); }
    if (this.getCoursesSub) { this.getCoursesSub.unsubscribe(); }
    if (this.addScheduleSub) { this.addScheduleSub.unsubscribe(); }
  }
}
