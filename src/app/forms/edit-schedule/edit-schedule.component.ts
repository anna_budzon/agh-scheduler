import { Component, OnDestroy, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { MatBottomSheet, MatSelectionList, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { AvailabilityResponse } from '../../models/availability-response';
import { Class, ClassTypeEnum } from '../../models/class';
import { ClassTime } from '../../models/class-time';
import { Day } from '../../models/day';
import { Room } from '../../models/room';
import { Schedule } from '../../models/schedule';
import { Teacher } from '../../models/teacher';
import { OptionMenuChoice, OptionMenuSheetComponent } from '../../pop-ups/option-menu-sheet/option-menu-sheet.component';
import { ScheduleService } from '../../services/schedule.service';
import { OccupationStateEnum } from '../../models/occupation-state.enum';
import { Conflict, ConflictTypeEnum } from '../../models/conflict';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';

@Component({
  selector: 'app-edit-schedule',
  templateUrl: './edit-schedule.component.html',
  styleUrls: ['./edit-schedule.component.scss']
})
export class EditScheduleComponent implements OnInit, OnDestroy {
  @ViewChild(MatSelectionList, { static: false }) selectionList: MatSelectionList;
  schedule: Schedule;
  classes: Class[] = [];
  routeSub: Subscription;
  selectedClass: Class | null = null;
  availability: AvailabilityResponse;
  classTime = [
    { id: 0, from: '8:00', to: '9:30' },
    { id: 1, from: '9:45', to: '11:15' },
    { id: 2, from: '11:30', to: '13:00' },
    { id: 3, from: '13:15', to: '14:45' },
    { id: 4, from: '15:00', to: '16:30' },
    { id: 5, from: '16:45', to: '18:15' },
    { id: 6, from: '18:30', to: '20:00' }
  ];
  availabilitySub: Subscription;
  editSub: Subscription;

  constructor(
    private scheduleService: ScheduleService,
    private route: ActivatedRoute,
    private menuOptions: MatBottomSheet,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    let scheduleId = '';
    this.routeSub = this.route.params
      .subscribe((params) => {
        scheduleId = params['id'];
      });
    this.scheduleService.getSchedule(scheduleId)
      .subscribe((schedule) => {
        this.schedule = schedule;
      });
  }

  getScheduledClasses(classes: Class[], elem: ClassTime) {
    return classes.filter(cls => cls.time.id === elem.id);
  }

  onSingleSelect(event) {
    const ifSelected = event.option.selected;
    this.selectionList.selectedOptions.clear();
    event.option.selected = ifSelected;

    if (ifSelected) {
      this.selectedClass = this.selectionList.selectedOptions.selected[0].value;

      this.availabilitySub = this.scheduleService.defineIfAvailable(this.selectedClass)
          .pipe(
            debounceTime(1000)
          )
          .subscribe((availability: AvailabilityResponse) => {
            if (this.selectedClass) {
              this.availability = { ...availability };
            } else {
              this.availability = null;
            }
          });
    } else {
      this.selectedClass = null;
      this.availability = null;
    }
  }

  getClassInfo(cls: Class) {
    // tslint:disable-next-line:max-line-length
    return `Grupa ${this.getStudentGroup(cls.type, cls.studentGroupNr)}, sala ${this.getRoom(cls.room)}, prowadzący ${this.getTeacher(cls.teacher)}`;
  }

  getShortClassInfo(cls: Class) {
    return `${cls.name} :
      G${this.getStudentGroup(cls.type, cls.studentGroupNr)}, ${this.getRoom(cls.room)}, ${this.getTeacher(cls.teacher)}`;
  }

  getStudentGroup(type: ClassTypeEnum, studentGroupNr) {
    let group = '';

    switch (type) {
      case ClassTypeEnum.IT:
      case ClassTypeEnum.LAB:
        group = `ĆL-${studentGroupNr.labGroups}`;
        break;
      case ClassTypeEnum.LECTURE:
        group = `W`;
        break;
      case ClassTypeEnum.PROJECT:
        group = `P-${studentGroupNr.regularGroups}`;
        break;
      case ClassTypeEnum.RECITATION:
        group = `A-${studentGroupNr.regularGroups}`;
        break;
      default:
        group = 'N/A';
    }
    return group;
  }

  getTeacher(teacher: Teacher) {
    return `${teacher.title} ${teacher.lastName}`;
  }

  getRoom(room: Room) {
    return `${room.building.name}-${room.number}`;
  }

  openMenuOptions(day: Day, time: ClassTime) {
    const scheduledClasses = day.classes
      .filter(cl => cl.time.from === time.from);

    const menuOptionsRef = this.menuOptions.open(OptionMenuSheetComponent, {
      data: {
        selectedClass: this.selectedClass,
        scheduledClasses: scheduledClasses,
        day: day,
        time: time,
        availability: this.availability
      }
    });

    menuOptionsRef.afterDismissed()
      .subscribe((result: { choice: OptionMenuChoice, classesToDelete: Class[] }) => {
        if (result) {
          switch (result.choice) {
            case OptionMenuChoice.ADD:
              this.scheduleClass(this.selectedClass, day, time);
              break;
            case OptionMenuChoice.DELETE:
              this.unScheduleClasses(result.classesToDelete, day);
              break;
            default:
          }
        }
      });
  }

  scheduleClass(selectedClass: Class, day: Day, time: ClassTime) {
    selectedClass.time = {
      day: day.dayType,
      id: time.id,
      from: time.from,
      to: time.to
    };
    const dayIdx = this.schedule.days.indexOf(day);
    this.schedule.days[dayIdx].classes.push(selectedClass);
    this.classes.splice(this.classes.indexOf(selectedClass), 1);
    this.selectedClass = null;
  }

  unScheduleClasses(classesToDelete: Class[], day: Day) {
    const dayIdx = this.schedule.days.indexOf(day);
    classesToDelete.forEach((cls: Class) => {
      const classIdx = this.schedule.days[dayIdx].classes.indexOf(cls);
      this.schedule.days[dayIdx].classes.splice(classIdx, 1);
      this.classes.push(cls);
    });
  }

  getAvailability(day: Day, time: ClassTime) {
    if (this.selectedClass && this.availability) {
      const classTime = this.availability.schedule[`${day.dayType}`].find(t => t.id === time.id);

      const isConflicts = this.checkConflictsWithCurrentClasses(day, time, this.selectedClass);

      if (classTime) {
        return (classTime.state !== OccupationStateEnum.OCCUPIED && isConflicts) ?
          OccupationStateEnum.OCCUPIED : classTime.state;
      }
    }

    return OccupationStateEnum.NONE;
  }

  checkConflictsWithCurrentClasses(day: Day, time: ClassTime, selectedClass: Class) {

    const currentD = this.schedule.days.find(d => d.dayType === day.dayType);
    const currentCls = currentD.classes.filter(cls => cls.time.id === time.id);
    if (currentCls && currentCls.length) {
      const teacherConflict = currentCls.some(cls => (cls.teacher as any)._id === (selectedClass.teacher as any)._id);
      const roomConflict = currentCls.some(cls => (cls.room as any)._id === (selectedClass.room as any)._id);
      const groupConflict = currentCls.some(cls => {
        return this.defineGroupConflict(cls, selectedClass);
      });

      if (groupConflict || teacherConflict || roomConflict) {
        const av = this.availability.schedule[`${day.dayType}`].find(t => t.id === time.id);
        av.state = OccupationStateEnum.OCCUPIED;
        if (!av.conflicts) { av.conflicts = []; }
        this.defineConflict(groupConflict, teacherConflict, roomConflict, av.conflicts);
      }

      return (teacherConflict || roomConflict || groupConflict);
    } else {
      return false;
    }
  }

  defineConflict(groupConflict, teacherConflict, roomConflict, conflicts: Conflict[]) {
    if (groupConflict && !conflicts.some(c => c.conflictType === ConflictTypeEnum.GROUP_BUSY)) {
      conflicts.push({ message: 'Konflikt pomiędzy grupami studentów.', conflictType: ConflictTypeEnum.GROUP_BUSY });
    }
    if (teacherConflict && !conflicts.some(c => c.conflictType === ConflictTypeEnum.TEACHER_BUSY)) {
      conflicts.push({ message: 'Prowadzący jest w tym czasie zajęty.', conflictType: ConflictTypeEnum.TEACHER_BUSY });
    }
    if (roomConflict && !conflicts.some(c => c.conflictType === ConflictTypeEnum.CLASS_OCCUPIED)) {
      conflicts.push({ message: 'Sala jest w tym czasie zajęta.', conflictType: ConflictTypeEnum.CLASS_OCCUPIED });
    }
  }

  defineGroupConflict(cls: Class, selectedClass: Class) {
    if (cls.type === ClassTypeEnum.LECTURE || selectedClass.type === ClassTypeEnum.LECTURE) {
      return true;
    } else if ((cls.type === ClassTypeEnum.LAB || cls.type === ClassTypeEnum.IT) &&
      (selectedClass.type === ClassTypeEnum.LAB || selectedClass.type === ClassTypeEnum.IT)) {
      return cls.studentGroupNr.labGroups === selectedClass.studentGroupNr.labGroups;
    } else if ((cls.type === ClassTypeEnum.PROJECT || cls.type === ClassTypeEnum.RECITATION) &&
      (selectedClass.type === ClassTypeEnum.PROJECT || selectedClass.type === ClassTypeEnum.RECITATION)) {
      return cls.studentGroupNr.regularGroups === selectedClass.studentGroupNr.regularGroups;
    } else {
      return false;
    }
  }

  editSchedule() {
    this.editSub = this.scheduleService.updateSchedule(this.schedule.id, this.schedule)
      .subscribe(() => {
        this.snackBar.openFromComponent(InformSnackBarComponent, {
          data: { message: 'Plan zajęć został zaktualizowany.' },
          duration: 2000,
          verticalPosition: 'bottom',
          horizontalPosition: 'left'
        });
    this.router.navigate([`/schedules/${this.schedule.id}`]);
      });
  }

  goBack() {
    this.router.navigate(['']);
  }

  ngOnDestroy() {
    if (this.routeSub) { this.routeSub.unsubscribe(); }
  }
}
