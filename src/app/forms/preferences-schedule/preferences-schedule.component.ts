import { Location } from '@angular/common';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatSnackBar, MatBottomSheet } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Class, ClassTypeEnum } from '../../models/class';
import { Room } from '../../models/room';
import { Schedule } from '../../models/schedule';
import { Teacher } from '../../models/teacher';
import { CommonService } from '../../services/common.service';
import { ScheduleService } from '../../services/schedule.service';
import { TeacherService } from '../../services/teacher.service';
import { ClassTime } from '../../models/class-time';
import { Day } from '../../models/day';
import { PreferencesMenuSheetComponent } from '../../pop-ups/preferences-menu-sheet/preferences-menu-sheet.component';
import { OptionMenuChoice } from '../../pop-ups/option-menu-sheet/option-menu-sheet.component';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';


@Component({
  selector: 'app-preferences-schedule',
  templateUrl: './preferences-schedule.component.html',
  styleUrls: ['./preferences-schedule.component.scss']
})
export class PreferencesScheduleComponent implements OnInit, OnDestroy {
  schedule: Schedule;
  nameInfo = '';
  classTime = [
    { id: 0, from: '8:00', to: '9:30' },
    { id: 1, from: '9:45', to: '11:15' },
    { id: 2, from: '11:30', to: '13:00' },
    { id: 3, from: '13:15', to: '14:45' },
    { id: 4, from: '15:00', to: '16:30' },
    { id: 5, from: '16:45', to: '18:15' },
    { id: 6, from: '18:30', to: '20:00' }
  ];
  routeSub: Subscription;
  scheduleSub: Subscription;
  teacherSub: Subscription;
  roomSub: Subscription;
  editSub: Subscription;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private scheduleService: ScheduleService,
    private teacherService: TeacherService,
    private commonService: CommonService,
    private menuOptions: MatBottomSheet,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    let scheduleId = '';
    this.routeSub = this.route.params
      .subscribe(params => {
        scheduleId = params['id'];
      });
    this.scheduleSub = this.scheduleService.getSchedule(scheduleId)
      .subscribe(schedule => {
        this.schedule = schedule;
        this.getNameInfo();
      });
  }

  getNameInfo() {
    if (this.schedule.teacherId) {
      this.teacherSub = this.teacherService.getTeacher(this.schedule.teacherId)
        .subscribe(teacher => {
          this.nameInfo = `${teacher.title} ${teacher.firstName} ${teacher.lastName}`;
        });
    } else {
      this.roomSub = this.commonService.getRoom(this.schedule.roomId)
        .subscribe(room => {
          this.nameInfo = `Budynek ${room.building.name}, sala ${room.number}`;
        });
    }
  }

  getClassDetails(currentCls: Class) {
    if (!currentCls.name) {
      return 'TERMIN ZAJĘTY';
    } else {
      return `${currentCls.name} : G${this.getStudentGroup(currentCls.type, currentCls.studentGroupNr)},
        ${this.getRoom(currentCls.room)}, ${this.getTeacher(currentCls.teacher)}`;
    }
  }

  getStudentGroup(type: ClassTypeEnum, studentGroupNr) {
    let group = '';

    switch (type) {
      case ClassTypeEnum.IT:
      case ClassTypeEnum.LAB:
        group = `ĆL-${studentGroupNr.labGroups}`;
        break;
      case ClassTypeEnum.LECTURE:
        group = `W`;
        break;
      case ClassTypeEnum.PROJECT:
        group = `P-${studentGroupNr.regularGroups}`;
        break;
      case ClassTypeEnum.RECITATION:
        group = `A-${studentGroupNr.regularGroups}`;
        break;
      default:
        group = 'N/A';
    }
    return group;
  }

  getTeacher(teacher: Teacher) {
    return `${teacher.title} ${teacher.lastName}`;
  }

  getRoom(room: Room) {
    return `${room.building.name}-${room.number}`;
  }

  getScheduledClasses(classes: Class[], elem: ClassTime) {
    if (classes) {
      return classes.filter(cls => cls.time.id === elem.id);
    } else {
      return [];
    }
  }

  openMenuOptions(day: Day, time: ClassTime) {
    const scheduledClasses = day.classes ? day.classes
      .filter(cl => cl.time.id === time.id) : [];

      console.log(day);
      console.log(time);
    const menuOptionsRef = this.menuOptions.open(PreferencesMenuSheetComponent, {
      data: {
        scheduledClasses: scheduledClasses,
        day: day,
        time: time
      }
    });

    menuOptionsRef.afterDismissed()
      .subscribe((result: { choice: OptionMenuChoice, classToDelete?: Class }) => {
        if (result) {
          switch (result.choice) {
            case OptionMenuChoice.ADD:
              this.scheduleOccupied(day, time);
              break;
            case OptionMenuChoice.DELETE:
              this.unScheduleOccupied(result.classToDelete, day);
              break;
            default:
          }
        }
      });
  }

  scheduleOccupied(day: Day, time: ClassTime) {
    const classTime = {
      day: day.dayType,
      id: time.id,
      from: time.from,
      to: time.to
    };

    const occupiedStatus = {
      time: classTime,
      scheduleId: this.schedule.id
    };

    const dayIdx = this.schedule.days.indexOf(day);
    this.schedule.days[dayIdx].classes.push(occupiedStatus);
  }

  unScheduleOccupied(cls: Class, day: Day) {
    const dayIdx = this.schedule.days.indexOf(day);
      const classIdx = this.schedule.days[dayIdx].classes.indexOf(cls);
      this.schedule.days[dayIdx].classes.splice(classIdx, 1);
  }

  editSchedule() {
    this.editSub = this.scheduleService.updatePreferencesSchedule(this.schedule)
      .subscribe(() => {
        this.snackBar.openFromComponent(InformSnackBarComponent, {
          data: { message: 'Plan zajęć został zaktualizowany.' },
          duration: 2000,
          verticalPosition: 'bottom',
          horizontalPosition: 'left'
        });
        this.router.navigate([`/schedules/${this.schedule.id}`]);
      });
  }

  goBack() {
    this.location.back();
   }

   ngOnDestroy() {
     if (this.routeSub) { this.routeSub.unsubscribe(); }
     if (this.scheduleSub) { this.scheduleSub.unsubscribe(); }
     if (this.roomSub) { this.roomSub.unsubscribe(); }
     if (this.teacherSub) { this.teacherSub.unsubscribe(); }
   }
}
