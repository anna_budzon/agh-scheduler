import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Teacher } from '../models/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  teachersUrl = 'http://localhost:3000/api/teacher';

  constructor(private http: HttpClient) { }

  getTeachers(departmentId: string) {
    return this.http.get<Teacher[]>(`${this.teachersUrl}/department/${departmentId}`)
      .pipe(
        map((teachers: any) => {
          return teachers.map(teacher => {
              return {
              ...teacher,
              schedule: { ...teacher.schedule, id: teacher.schedule._id },
              id: teacher._id
              };
            });
          })
        );
  }

  getTeacher(teacherId: string) {
    return this.http.get<Teacher>(`${this.teachersUrl}/${teacherId}`);
  }

  getAllTeachers() {
    return this.http.get<Teacher[]>(this.teachersUrl)
      .pipe(
        map((teachers: any) => {
          return teachers.map(teacher => {
              return {
              ...teacher,
              schedule: { ...teacher.schedule, id: teacher.schedule._id },
              id: teacher._id
              };
            });
          })
        );
  }

  addTeacher(newTeacher: Teacher) {
    return this.http.post<Teacher>(this.teachersUrl, newTeacher);
  }

  updateTeacher(updatedTeacher: Teacher) {
    return this.http.put<Teacher>(`${this.teachersUrl}/${updatedTeacher.id}`, updatedTeacher);
  }

  deleteTeacher(teacherId: string) {
    return this.http.delete(`${this.teachersUrl}/${teacherId}`);
  }
}
