import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Course } from '../models/course';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  coursesUrl = 'http://localhost:3000/api/course';

  constructor(private http: HttpClient) { }

  getAllCourses(departmentId: string) {
    return this.http.get<Course[]>(`${this.coursesUrl}/department/${departmentId}`)
      .pipe(
        map((courses: any) => {
          return courses.map(course => {
              return {
              ...course,
              id: course._id
              };
            });
          })
        );
  }

  getParticularCourses(groupMajorId: string) {
    return this.http.get<Course[]>(`${this.coursesUrl}/group/${groupMajorId}`)
      .pipe(
        map((courses: any) => {
          return courses.map(course => {
              return {
              ...course,
              id: course._id
              };
            });
          })
        );
  }

  addCourse(newCourse: Course) {
    return this.http.post<Course>(this.coursesUrl, newCourse);
  }

  updateCourse(updatedCourse: Course) {
    return this.http.put<Course>(`${this.coursesUrl}/${updatedCourse.id}`, updatedCourse);
  }

  deleteCourse(courseId: string) {
    return this.http.delete(`${this.coursesUrl}/${courseId}`);
  }
}
