import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Major } from '../models/major';
import { MajorGroup } from '../models/major-group';


@Injectable({
  providedIn: 'root'
})
export class MajorService {
  majorsUrl = 'http://localhost:3000/api/major';

  constructor(private http: HttpClient) { }

  getMajors(departmentId: string) {
    return this.http.get(`${this.majorsUrl}/department/${departmentId}`)
    .pipe(
      map((majors: any) => {
        return majors.map(major => {
            return {
              ...major,
              id: major._id
            };
        });
      })
    );
  }

  getMajor(majorId: string) {
    return this.http.get(`${this.majorsUrl}/${majorId}`);
  }

  addMajor(majorName: string, departmentId: string) {
    return this.http.post<Major>(this.majorsUrl,
      { name: majorName, departmentId: departmentId });
  }

  updateMajor(major: Major) {
    return this.http.put<Major>(`${this.majorsUrl}/${major.id}`, major);
  }

  deleteMajor(majorId: string) {
    return this.http.delete(`${this.majorsUrl}/${majorId}`);
  }

  getMajorGroup(majorId: string, groupId: string) {
    return this.http.get(`${this.majorsUrl}/${majorId}/group/${groupId}`);
  }

  getMajorGroups(majorId: string) {
    return this.http.get<MajorGroup[]>(`${this.majorsUrl}/${majorId}/group/`)
      .pipe(
        map((groups: any) => {
          return groups.map(group => {
              return {
                ...group,
                id: group._id
              };
          });
        })
      );
  }

  addMajorGroup(majorId: string, newMajorGroup: MajorGroup) {
    return this.http.post<MajorGroup>(`${this.majorsUrl}/${majorId}/group/`, newMajorGroup);
  }

  updateMajorGroup(majorId: string, groupId: string, updatedMajorGroup: MajorGroup) {
    return this.http.put<MajorGroup>(`${this.majorsUrl}/${majorId}/group/${groupId}`, updatedMajorGroup);
  }

  deleteMajorGroup(majorId: string, groupId: string) {
    return this.http.delete(`${this.majorsUrl}/${majorId}/group/${groupId}`);
  }
}
