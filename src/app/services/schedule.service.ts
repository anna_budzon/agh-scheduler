import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AvailabilityResponse } from '../models/availability-response';
import { Class } from '../models/class';
import { Schedule } from '../models/schedule';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  schedulesUrl = 'http://localhost:3000/api/schedule';

  constructor(private http: HttpClient) { }

  getSchedule(scheduleId: string) {
    return this.http.get<Schedule>(`${this.schedulesUrl}/${scheduleId}`)
     .pipe(
        map((schedule: any) => {
              return {
              ...schedule,
              id: schedule._id
              };
          })
        );
  }

  getSchedules(departmentId: string) {
    return this.http.get<Schedule[]>(`${this.schedulesUrl}/department/${departmentId}`)
      .pipe(
        map((schedules: any) => {
          return schedules.map(schedule => {
              return {
              ...schedule,
              id: schedule._id
              };
            });
          })
        );
  }

  initializeSchedule(newSchedule: Schedule) {
    return this.http.post<Schedule>(this.schedulesUrl, newSchedule);
  }

  createSchedule(createdSchedule: Schedule) {
    return this.http.put<Schedule>(`${this.schedulesUrl}/${createdSchedule.id}/create`, createdSchedule);
  }

  updateSchedule(id: string, updatedSchedule: Schedule) {
    return this.http.put<Schedule>(`${this.schedulesUrl}/${id}`, updatedSchedule);
  }

  updatePreferencesSchedule(schedule: Schedule) {
      return this.http.put<Schedule>(`${this.schedulesUrl}/${schedule.id}/preferences`, schedule);
  }

  deleteSchedule(scheduleId: string) {
    return this.http.delete(`${this.schedulesUrl}/${scheduleId}`);
  }

  addClasses(scheduleId: string, newClasses: Class[]) {
    return this.http.post<Class[]>(`${this.schedulesUrl}/${scheduleId}/classes`, newClasses);
  }

  updateClasses(scheduleId: string, updatedClasses: Class[]) {
    return this.http.put<Class>(`${this.schedulesUrl}/${scheduleId}/classes`, updatedClasses);
  }

  getClasses(scheduleId: string) {
  // getClasses() {
    return this.http.get<Class[]>(`${this.schedulesUrl}/${scheduleId}/classes`)
      .pipe(
        map((classes: any) => {
          return classes.map(cl => {
              return {
              ...cl,
              id: cl._id
              };
            });
          })
        );
  }

  getClass(scheduleId: string, classId: string) {
    return this.http.get<Class>(`${this.schedulesUrl}/${scheduleId}/classes/${classId}`);
  }

  defineIfAvailable(selectedClass: Class) {
    return this.http.post<AvailabilityResponse>(`${this.schedulesUrl}/availability`, selectedClass);
  }
}
