import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Building } from '../models/building';
import { Room } from '../models/room';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  buildingUrl = 'http://localhost:3000/api/building';
  roomUrl = 'http://localhost:3000/api/room';

  constructor(private http: HttpClient) {}

  getBuildings() {
    return this.http.get<Building[]>(this.buildingUrl).pipe(
      map((buildings: any) => {
        return buildings.map(build => {
          return {
            ...build,
            id: build._id,
            rooms: build.rooms.map(room => {
              return {
                ...room,
                id: room._id
              };
            })
          };
        });
      })
    );
  }

  getRoom(roomId: string) {
    return this.http.get<Room>(`${this.roomUrl}/${roomId}`);
  }


  getRooms(departmentId: string) {
    return this.http.get<Room[]>(`${this.roomUrl}/department/${departmentId}`).pipe(
      map((rooms: any) => {
        return rooms.map(room => {
          return {
            ...room,
            schedule: { ...room.schedule, id: room.schedule._id },
            id: room._id
          };
        });
      })
    );
  }
}
