import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Student } from '../models/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  studentsUrl = 'http://localhost:3000/api/student';

  constructor(private http: HttpClient) { }

  getStudents(majorGroupId: string) {
    return this.http.get<Student[]>(`${this.studentsUrl}/group/${majorGroupId}`)
      .pipe(
        map((students: any) => {
          return students.map(student => {
              return {
              ...student,
              id: student._id
              };
            });
          })
        );
  }

  getAllStudents(departmentId: string) {
    return this.http.get<Student[]>(`${this.studentsUrl}/department/${departmentId}`)
      .pipe(
        map((students: any) => {
          return students.map(student => {
              return {
              ...student,
              id: student._id
              };
            });
          })
        );
  }

  addStudent(newStudent: Student) {
    return this.http.post<Student>(this.studentsUrl, newStudent);
  }

  updateStudent(updatedStudent: Student) {
    return this.http.put<Student>(`${this.studentsUrl}/${updatedStudent.id}`, updatedStudent);
  }

  deleteStudent(studentId: string) {
    return this.http.delete(`${this.studentsUrl}/${studentId}`);
  }
}
