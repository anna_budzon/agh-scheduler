import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Department } from '../models/department';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  departUrl = 'http://localhost:3000/api/department';

  constructor(private http: HttpClient) { }

  getDepartment(departmentId: string) {
    return this.http.get<Department>(`${this.departUrl}/${departmentId}`);
  }

  setDepartment(department: Department) {
    return this.http.post<Department>(`${this.departUrl}`, department);
   }

}
