import { Component, Inject } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { Class, ClassTypeEnum } from '../../models/class';
import { Day } from '../../models/day';
import { TranslateToPlPipe } from '../../pipes/translate-to-pl.pipe';
import { ClassTime } from '../../models/class-time';
import { AvailabilityResponse } from '../../models/availability-response';
import { OccupationStateEnum } from '../../models/occupation-state.enum';

export const enum OptionMenuChoice {
  ADD = 'ADD',
  DELETE = 'DELETE',
}

@Component({
  selector: 'app-option-menu-sheet',
  templateUrl: './option-menu-sheet.component.html',
  styleUrls: ['./option-menu-sheet.component.scss']
})
export class OptionMenuSheetComponent {
  classesToDelete: Class[] = [];
  constructor(
    private translatePipe: TranslateToPlPipe,
    private bottomSheetRef: MatBottomSheetRef<OptionMenuSheetComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: {
      selectedClass: Class;
      scheduledClasses: Class[];
      day: Day;
      time: ClassTime,
      availability: AvailabilityResponse
    }
  ) {}

  openLink(event: MouseEvent, choice?: OptionMenuChoice): void {
    if (choice === OptionMenuChoice.ADD) {
      this.bottomSheetRef.dismiss({ choice: choice });
    } else if (choice === OptionMenuChoice.DELETE && this.classesToDelete && this.classesToDelete.length) {
      this.bottomSheetRef.dismiss({ choice: choice, classesToDelete: this.classesToDelete });
    } else {
      this.bottomSheetRef.dismiss();
    }
    event.preventDefault();
  }

  getDayAndTime() {
    if (this.data.day && this.data.time) {
      return `${this.translatePipe.transform(this.data.day.dayType)} (${this.data.time.from} - ${this.data.time.to})`;
    } else {
      return '';
    }
  }


  getClass(scheduledClass?: Class) {
    let cls: Class;
    if (scheduledClass) {
      cls = scheduledClass;
    } else {
      cls = this.data.selectedClass;
    }

    let group = '';

    switch (cls.type) {
      case ClassTypeEnum.IT:
      case ClassTypeEnum.LAB:
        group = `ĆL-${cls.studentGroupNr.labGroups}`;
        break;
      case ClassTypeEnum.LECTURE:
        group = `W`;
        break;
      case ClassTypeEnum.PROJECT:
        group = `P-${cls.studentGroupNr.regularGroups}`;
        break;
      case ClassTypeEnum.RECITATION:
        group = `A-${cls.studentGroupNr.regularGroups}`;
        break;
      default:
        group = 'N/A';
    }

    return `${cls.name} - ${group}, ${cls.room.building.name}-${cls.room.number}, ${cls.teacher.title} ${cls.teacher.lastName}.`;
  }

  preventBubbling(event) {
    event.stopPropagation();
  }

  changeClassesToDelete(event, cls: Class) {
    if (event.checked) {
      this.classesToDelete.push(cls);
    } else {
      this.classesToDelete.splice(this.classesToDelete.indexOf(cls), 1);
    }
  }

  checkIfAvailable() {
    if (this.data.availability && this.data.day && this.data.time) {
      const classTime = this.data.availability.schedule[`${this.data.day.dayType}`]
        .find(t => t.id === this.data.time.id);
        return classTime ? (
          +classTime.state === OccupationStateEnum.OCCUPIED && classTime.conflicts.length) : true;
    }
  }

  getConflicts() {
      const classTime = this.data.availability.schedule[`${this.data.day.dayType}`]
        .find(t => t.id === this.data.time.id);
        return classTime.conflicts;
  }
}
