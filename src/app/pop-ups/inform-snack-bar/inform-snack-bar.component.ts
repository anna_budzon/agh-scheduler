import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
  selector: 'app-inform-snack-bar',
  templateUrl: './inform-snack-bar.component.html',
  styleUrls: ['./inform-snack-bar.component.scss']
})
export class InformSnackBarComponent {

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: { message: string }
  ) { }
}
