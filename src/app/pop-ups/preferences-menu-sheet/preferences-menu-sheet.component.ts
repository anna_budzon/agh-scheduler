import { Component, OnInit, Inject } from '@angular/core';
import { TranslateToPlPipe } from '../../pipes/translate-to-pl.pipe';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { OptionMenuSheetComponent, OptionMenuChoice } from '../option-menu-sheet/option-menu-sheet.component';
import { Class, ClassTypeEnum } from '../../models/class';
import { Day } from '../../models/day';
import { ClassTime } from '../../models/class-time';
import { AvailabilityResponse } from '../../models/availability-response';
import { OccupationStateEnum } from '../../models/occupation-state.enum';

@Component({
  selector: 'app-preferences-menu-sheet',
  templateUrl: './preferences-menu-sheet.component.html',
  styleUrls: ['./preferences-menu-sheet.component.scss']
})
export class PreferencesMenuSheetComponent implements OnInit {
  classes: Class[];
  occupied = false;

  constructor(
    private translatePipe: TranslateToPlPipe,
    private bottomSheetRef: MatBottomSheetRef<OptionMenuSheetComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: {
      scheduledClasses: Class[];
      day: Day;
      time: ClassTime,
    }
  ) {}

  ngOnInit() {
    if (this.data && this.data.scheduledClasses) {
      this.classes = this.data.scheduledClasses.filter(cls => cls.name);
      this.occupied = this.data.scheduledClasses.some(cls => !cls.name);
    }
  }

  openLink(event: MouseEvent, choice?: OptionMenuChoice): void {
    if (choice === OptionMenuChoice.ADD) {
      this.bottomSheetRef.dismiss({ choice: choice });
    } else if (choice === OptionMenuChoice.DELETE) {
      const classToDelete = this.getOccupiedClass();
      this.bottomSheetRef.dismiss({ choice: choice, classToDelete: classToDelete });
    } else {
      this.bottomSheetRef.dismiss();
    }
    event.preventDefault();
  }

  getDayAndTime() {
    if (this.data.day && this.data.time) {
      return `${this.translatePipe.transform(this.data.day.dayType)} (${this.data.time.from} - ${this.data.time.to})`;
    } else {
      return '';
    }
  }

  getOccupiedClass() {
    return this.data.scheduledClasses.find(cls => !cls.name);
  }
}
