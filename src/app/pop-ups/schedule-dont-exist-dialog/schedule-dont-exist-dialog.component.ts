import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-schedule-dont-exist-dialog',
  templateUrl: './schedule-dont-exist-dialog.component.html',
  styleUrls: ['./schedule-dont-exist-dialog.component.scss']
})
export class ScheduleDontExistDialogComponent {

  constructor(public dialogRef: MatDialogRef<ScheduleDontExistDialogComponent>) {}

  onConfirm() {
    this.dialogRef.close(true);
  }

  onCancel() {
    this.dialogRef.close(false);
  }
}
