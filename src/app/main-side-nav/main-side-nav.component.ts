import { Component } from '@angular/core';

@Component({
  selector: 'app-main-side-nav',
  templateUrl: './main-side-nav.component.html',
  styleUrls: ['./main-side-nav.component.scss']
})
export class MainSideNavComponent {

  navMenu = [
    { name: 'Wydział', link: 'department', icon: 'account_balance'},
    { name: 'Kierunki', link: '/majors', icon: 'group_add' },
    { name: 'Uczniowie', link: '/students', icon: 'school' },
    { name: 'Nauczyciele', link: '/teachers', icon: 'supervisor_account' },
    { name: 'Sale', link: '/rooms', icon: 'meeting_room' },
    { name: 'Przedmioty', link: '/courses', icon: 'list_alt' },
    { name: 'Podziały godzin', link: '/schedules', icon: 'event_note' },
  ];
}
