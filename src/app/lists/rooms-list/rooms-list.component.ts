import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Building } from '../../models/building';
import { Room, RoomTypeEnum } from '../../models/room';
import { CommonService } from '../../services/common.service';
import { Globals } from '../../../environments/globals';
import { ErrorSnackBarComponent } from '../../pop-ups/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.scss']
})
export class RoomsListComponent implements OnInit, OnDestroy {

  dataSource = new MatTableDataSource<Room>([]);
  building = new FormControl('');
  rooms: Room[];
  buildings: Building[];
  filterValue: string;
  getRoomsSub: Subscription;
  buildingsSub: Subscription;

  constructor(
    private commonService: CommonService,
    private router: Router,
    private snackBar: MatSnackBar,
    private globals: Globals,
  ) { }

  ngOnInit() {
    this.getRoomsSub = this.commonService
      .getRooms(this.globals.department.id)
      .subscribe((fetchedRooms: Room[]) => {
        this.rooms = fetchedRooms;
        this.refresh();
      });

    this.buildingsSub = this.commonService.getBuildings()
      .subscribe((buildings: Building[]) => {
        this.buildings = buildings;
      });

    this.building.valueChanges
      .subscribe((building: Building) => {
          this.filterValue = building ? building.name : '';
          this.dataSource.filter = JSON.stringify(this.filterValue);
        }
      );
  }

  getColumns() {
    return ['number', 'building', 'type', 'limitOfPlaces', 'displaySchedule'];
  }

  getRoomType(type: RoomTypeEnum) {
    let roomType = '';
    switch (type) {
      case RoomTypeEnum.OFFICE:
        roomType = 'Gabinet';
        break;
      case RoomTypeEnum.IT:
        roomType = 'Sala komputerowa';
        break;
      case RoomTypeEnum.LECTURE:
        roomType = 'Sala wykładowa';
        break;
      case RoomTypeEnum.LAB:
        roomType = 'Sala laboratoryjna';
        break;
      case RoomTypeEnum.PE:
        roomType = 'Sala gimnastyczna';
        break;
      case RoomTypeEnum.SPECIAL:
        roomType = 'Specjalna';
        break;
      default:
        roomType = 'Sala audytoryjna';
        break;
    }
    return roomType;
  }

  // addTeacher() {
  //   const dialogRef = this.dialog.open(AddTeacherDialogComponent, {
  //     width: '600px'
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe((newTeacher: Teacher) => {
  //       if (newTeacher) {
  //         this.addTeacherSub = this.teacherService.addTeacher(newTeacher)
  //           .subscribe((addedTeacher: Teacher) => {
  //             this.teachers.push(addedTeacher);
  //             this.refresh();
  //             this.snackBar.open(
  //               `Nauczyciel ${addedTeacher.firstName} ${addedTeacher.lastName} został dodany.`, null, {
  //                 duration: 2000,
                      // verticalPosition: 'bottom',
                      // horizontalPosition: 'left'
  //             });
  //           });
  //       }
  //     });
  // }

  // editRoom(roomToUpdate: Room) {
  //   const dialogRef = this.dialog.open(AddTeacherDialogComponent, {
  //     width: '600px',
  //     data: roomToUpdate
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe((editedRoom: Room) => {
  //       if (editedRoom) {
  //       this.updateRoomSub = this.commonService.updateRoom(editedRoom)
  //         .subscribe((updateRoom: Room) => {
  //           const idx = this.rooms.findIndex((st: Room) =>
  //             st.id === updateRoom.id
  //           );
  //           this.rooms.splice(idx, 1, updateRoom);
  //           this.refresh();
  //           this.snackBar.open(
  //             `Sala ${updateRoom.number} została uaktualniona.`,
  //             null,
  //             { duration: 2000,
// verticalPosition: 'bottom',
// horizontalPosition: 'left' }
  //           );
  //         });
  //       }
  //     });
  // }

  // deleteTeacher(teacherToDelete: Teacher) {
  //   const dialogRef = this.dialog.open(DeleteTeacherDialogComponent, {
  //     width: '400px',
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe((result) => {
  //       if (result) {
  //         this.deleteTeacherSub = this.teacherService.deleteTeacher(teacherToDelete.id)
  //           .subscribe(() => {
  //             const idx = this.teachers.findIndex((st: Teacher) =>
  //               st.id === teacherToDelete.id
  //             );
  //             this.teachers.splice(idx, 1);
  //             this.refresh();
  //             this.snackBar.open(`Nauczyciel ${teacherToDelete.firstName} ${teacherToDelete.lastName} został usunięty.`,
  //             null,
  //             { duration: 2000,
    // verticalPosition: 'bottom',
    // horizontalPosition: 'left' });
  //           });
  //       }
  //     });
  // }

  displaySchedule(room: Room) {
    if (room.schedule) {
      this.router.navigate([`schedules/${room.schedule.id}`]);
    } else {
      this.snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: { message: 'Sala nie ma jeszcze przygotowanego planu zajęć!' },
        duration: 2000,
        verticalPosition: 'bottom',
        horizontalPosition: 'left'
      });
    }
  }

  goBack() {
    this.router.navigate(['']);
  }

  refresh() {
    this.dataSource.data = this.rooms;
    this.dataSource.filterPredicate = this.tableFilter();
  }

  tableFilter(): (data: any, filter: string) => boolean {
    const filterFunction = function(data, filter): boolean {
      const searchTerms = JSON.parse(filter);
      return data.building.name.indexOf(searchTerms) !== -1;
    };
    return filterFunction;
  }

  ngOnDestroy() {
    if (this.getRoomsSub) { this.getRoomsSub.unsubscribe(); }
  }
}
