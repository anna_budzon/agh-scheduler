import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Course } from '../../../models/course';
import { Major, MajorModeEnum, MajorTypeEnum } from '../../../models/major';
import { AddStudentDialogComponent } from '../../students-list/add-student-dialog/add-student-dialog.component';

@Component({
  selector: 'app-add-course-dialog',
  templateUrl: './add-course-dialog.component.html',
  styleUrls: ['./add-course-dialog.component.scss']
})
export class AddCourseDialogComponent {
  newCourseForm = new FormGroup({
    name: new FormControl(this.data.course ? this.data.course.name : '', [
      Validators.required
    ]),
    major: new FormControl(this.data.course ? this.data.course.major : '', [
      Validators.required
    ]),
    majorType: new FormControl(this.data.course ? this.data.course.majorType : '', [
      Validators.required
    ]),
    majorMode: new FormControl(this.data.course ? this.data.course.majorMode : '', [
      Validators.required
    ]),
    semester: new FormControl(this.data.course ? +this.data.course.semester : '', [
      Validators.required,
      Validators.pattern('^[0-9]*$')
    ]),
  });

  hoursForm = new FormGroup({
    lecture: new FormControl(this.data.course ? this.data.course.hours.lecture : 0, [ Validators.pattern('^[0-9]*$')]),
    lab: new FormControl(this.data.course ? this.data.course.hours.lab : 0, Validators.pattern('^[0-9]*$')),
    itLab: new FormControl(this.data.course ? this.data.course.hours.itLab : 0, Validators.pattern('^[0-9]*$')),
    recitation: new FormControl(this.data.course ? this.data.course.hours.recitation : 0, Validators.pattern('^[0-9]*$')),
    project: new FormControl(this.data.course ? this.data.course.hours.project : 0,  Validators.pattern('^[0-9]*$')),
    different: new FormControl(this.data.course ? this.data.course.hours.different : 0,  Validators.pattern('^[0-9]*$')),
  });
  types = [ MajorTypeEnum.FIRST, MajorTypeEnum.SECOND ];
  modes = [ MajorModeEnum.FULL_TIME, MajorModeEnum.PART_TIME ];

  constructor(
    public dialogRef: MatDialogRef<AddStudentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      majors: Major[],
      semesters: string[],
      departmentId: string,
      course?: Course
    }
  ) {}

  onSaveStudent() {
    this.newCourseForm.controls.name.markAsTouched();
    this.newCourseForm.controls.major.markAsTouched();
    this.newCourseForm.controls.semester.markAsTouched();
    this.newCourseForm.controls.majorType.markAsTouched();
    this.newCourseForm.controls.majorMode.markAsTouched();
    this.hoursForm.controls.lecture.markAsTouched();
    this.hoursForm.controls.lab.markAsTouched();
    this.hoursForm.controls.itLab.markAsTouched();
    this.hoursForm.controls.project.markAsTouched();
    this.hoursForm.controls.different.markAsTouched();
    this.hoursForm.controls.recitation.markAsTouched();

    if (this.newCourseForm.valid && this.hoursForm.valid) {
      this.dialogRef.close({
        ...this.newCourseForm.value,
        hours: { ...this.hoursForm.value },
        departmentId: this.data.departmentId,
        id: this.data.course ? this.data.course.id : null
      });
    }
  }

  onCancel() {
    this.dialogRef.close();
  }
}
