import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DeleteMajorDialogComponent } from '../../majors-list/delete-major-dialog/delete-major-dialog.component';

@Component({
  selector: 'app-delete-course-dialog',
  templateUrl: './delete-course-dialog.component.html',
  styleUrls: ['./delete-course-dialog.component.scss']
})
export class DeleteCourseDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteMajorDialogComponent>) {}

  onConfirm() {
    this.dialogRef.close(true);
  }

  onCancel() {
    this.dialogRef.close(false);
  }
}
