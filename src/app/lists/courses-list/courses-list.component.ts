import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Course } from '../../models/course';
import { Major } from '../../models/major';
import { CourseService } from '../../services/course.service';
import { MajorService } from '../../services/major.service';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { AddCourseDialogComponent } from './add-course-dialog/add-course-dialog.component';
import { DeleteCourseDialogComponent } from './delete-course-dialog/delete-course-dialog.component';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource<Course>([]);
  major = new FormControl('');
  semester = new FormControl('');
  courses: Course[];
  majors: Major[];
  departmentId = '5c6431f49132f3266097476d';
  semesters = [1, 2, 3, 4, 5, 6, 7, 8];
  filterValue = {
    semester: '',
    major: ''
  };
  getCoursesSub: Subscription;
  majorsSub: Subscription;
  addCourseSub: Subscription;
  updateCourseSub: Subscription;
  deleteCourseSub: Subscription;

  constructor(
    private router: Router,
    private courseService: CourseService,
    private majorService: MajorService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.getCoursesSub = this.courseService.getAllCourses(this.departmentId)
      .subscribe((fetchedCourses: Course[]) => {
        this.courses = fetchedCourses;
        this.refresh();
      });

    this.majorsSub = this.majorService.getMajors(this.departmentId)
      .subscribe((majors: Major[]) => {
        this.majors = majors;
      });

    this.semester.valueChanges
      .subscribe((sem: string) => {
        this.filterValue.semester = sem ? sem : '';
        this.dataSource.filter = JSON.stringify(this.filterValue);
      });

    this.major.valueChanges
      .subscribe((major: Major) => {
        this.filterValue.major = major ? major.id : '';
        this.dataSource.filter = JSON.stringify(this.filterValue);
      });
  }

  getColumns() {
    return [
      'name',
      'major',
      'majorType',
      'majorMode',
      'semester',
      'lecture',
      'lab',
      'itLab',
      'recitation',
      'project',
      'different',
      'edit',
      'delete'
    ];
  }

  goBack() {
    this.router.navigate(['']);
  }

  refresh() {
    this.dataSource.data = this.courses;
    this.dataSource.filterPredicate = this.tableFilter();
  }

  tableFilter(): (data: any, filter: string) => boolean {
    const filterFunction = function(data, filter): boolean {
      const searchTerms = JSON.parse(filter);
      if (searchTerms.major && searchTerms.semester) {
        return data.major._id.indexOf(searchTerms.major) !== -1 && +data.semester === searchTerms.semester;
      } else if (searchTerms.semester) {
        return +data.semester === searchTerms.semester;
      } else {
        return data.major._id.indexOf(searchTerms.major) !== -1;
      }
    };
    return filterFunction;
  }

  addCourse() {
    const dialogRef = this.dialog.open(AddCourseDialogComponent, {
      width: '600px',
      data: {
        majors: this.majors,
        semesters: this.semesters,
        departmentId: this.departmentId
      }
    });

    dialogRef.afterClosed()
      .subscribe((newCourse: Course) => {
        if (newCourse) {
          this.addCourseSub = this.courseService.addCourse(newCourse)
            .subscribe((addedCourse: Course) => {
              this.courses.push(addedCourse);
              this.refresh();
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: `Przedmiot ${addedCourse.name} został dodany.` },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
            });
        }
      });
  }

  editCourse(courseToEdit: Course) {
    const dialogRef = this.dialog.open(AddCourseDialogComponent, {
      width: '600px',
      data: {
        course: courseToEdit,
        majors: this.majors,
        semesters: this.semesters,
        departmentId: this.departmentId
      }
    });

    dialogRef.afterClosed().subscribe((editedCourse: Course) => {
      if (editedCourse) {
        this.updateCourseSub = this.courseService.updateCourse(editedCourse)
          .subscribe((updatedCourse: Course) => {
            const idx = this.courses.findIndex((c: Course) => c.id === editedCourse.id);
            this.courses.splice(idx, 1, editedCourse);
            this.refresh();
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: `Przedmiot ${updatedCourse.name} został uaktualniony.` },
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'left'
            });
          });
      }
    });
  }

  deleteCourse(courseToDelete: Course) {
    const dialogRef = this.dialog.open(DeleteCourseDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteCourseSub = this.courseService.deleteCourse(courseToDelete.id)
          .subscribe(() => {
            const idx = this.courses.findIndex((c: Course) => c.id === courseToDelete.id);
            this.courses.splice(idx, 1);
            this.refresh();
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: `Student ${courseToDelete.name} został usunięty.` },
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'left'
            });
          });
      }
    });
  }

  ngOnDestroy() {
    if (this.getCoursesSub) {
      this.getCoursesSub.unsubscribe();
    }
    if (this.addCourseSub) {
      this.addCourseSub.unsubscribe();
    }
    if (this.majorsSub) {
      this.majorsSub.unsubscribe();
    }
    if (this.updateCourseSub) {
      this.updateCourseSub.unsubscribe();
    }
    if (this.deleteCourseSub) {
      this.deleteCourseSub.unsubscribe();
    }
  }
}
