import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Student } from '../../models/student';
import { StudentService } from '../../services/student.service';
import { Globals } from '../../../environments/globals';

@Component({
  selector: 'app-all-students-list',
  templateUrl: './all-students-list.component.html',
  styleUrls: ['./all-students-list.component.scss']
})
export class AllStudentsListComponent implements OnInit, OnDestroy {

  dataSource = new MatTableDataSource<Student>([]);
  students: Student[];
  filterValue: string;
  getStudentsSub: Subscription;

  constructor(
    private studentService: StudentService,
    private router: Router,
    private globals: Globals
  ) { }

  ngOnInit() {
    this.getStudentsSub = this.studentService.getAllStudents(this.globals.department.id)
      .subscribe((fetchedStudents: Student[]) => {
        this.students = fetchedStudents;
        this.refresh();
      });
  }

  getColumns() {
    return ['index', 'firstName', 'lastName', 'pesel', 'major'];
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  goBack() {
    this.router.navigate(['']);
  }

  refresh() {
    this.dataSource.data = this.students;
  }

  ngOnDestroy() {
    if (this.getStudentsSub) { this.getStudentsSub.unsubscribe(); }
  }
}
