import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Major, MajorModeEnum, MajorTypeEnum } from '../../models/major';
import { MajorGroup } from '../../models/major-group';
import { MajorService } from '../../services/major.service';
import { Globals } from '../../../environments/globals';
import { MatDialog } from '@angular/material';
import { ScheduleDontExistDialogComponent } from '../../pop-ups/schedule-dont-exist-dialog/schedule-dont-exist-dialog.component';

@Component({
  selector: 'app-schedules-list',
  templateUrl: './schedules-list.component.html',
  styleUrls: ['./schedules-list.component.scss']
})
export class SchedulesListComponent implements OnInit, OnDestroy {
  majorForm = new FormGroup({
    major: new FormControl('', [ Validators.required ])
  });

  groupForm = new FormGroup({
    group: new FormControl('', [ Validators.required ])
  });
  majors: Major[];
  majorGroups: MajorGroup[];
  getMajorsSub: Subscription;
  getGroupsSub: Subscription;

  constructor(
    private majorService: MajorService,
    private router: Router,
    private dialog: MatDialog,
    private globals: Globals
  ) {}

  ngOnInit() {
    this.getMajorsSub = this.majorService.getMajors(this.globals.department.id)
      .subscribe((majors: Major[]) => {
        this.majors = majors;
      });
  }

  fetchGroups() {
    if (this.majorForm.valid) {
      this.getGroupsSub = this.majorService.getMajorGroups(this.majorForm.value.major.id)
      .subscribe((groups: MajorGroup[]) => {
        this.majorGroups = groups;
      });
    }
  }

  getGroupInfo(group: MajorGroup) {
    const type = group.type === MajorTypeEnum.FIRST ? 'I stopień' : 'II stopień';
    const mode = group.mode === MajorModeEnum.FULL_TIME ? 'stacjonarne' : 'niestacjonarne';
    return `Rocznik ${group.beginYear}/${+group.beginYear + 1}, ${type}, ${mode}, semestr ${group.semester}, rok ${group.year}`;
  }

  loadSchedule() {
    if (this.groupForm.value.group.schedule) {
      this.router.navigate([`/schedules/${this.groupForm.value.group.schedule}`]);
    } else {
      const dialogRef = this.dialog.open(ScheduleDontExistDialogComponent);

      dialogRef.afterClosed()
        .subscribe((result) => {
          if (result) {
            this.router.navigate(['/schedules/create']);
          }
        });
    }
  }

  createSchedule() {
    this.router.navigate(['/schedules/create']);
  }

  goBack() {
    this.router.navigate(['']);
  }

  ngOnDestroy() {
    if (this.getGroupsSub) { this.getGroupsSub.unsubscribe(); }
    if (this.getMajorsSub) { this.getMajorsSub.unsubscribe(); }
  }
}
