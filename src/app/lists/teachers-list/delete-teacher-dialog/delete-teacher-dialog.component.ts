import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DeleteMajorDialogComponent } from '../../majors-list/delete-major-dialog/delete-major-dialog.component';

@Component({
  selector: 'app-delete-teacher-dialog',
  templateUrl: './delete-teacher-dialog.component.html',
  styleUrls: ['./delete-teacher-dialog.component.scss']
})
export class DeleteTeacherDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteMajorDialogComponent>) {}

  onConfirm() {
    this.dialogRef.close(true);
  }

  onCancel() {
    this.dialogRef.close(false);
  }
}
