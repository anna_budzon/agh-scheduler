import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTeacherDialogComponent } from './delete-teacher-dialog.component';
import { AppModule } from '../../../app.module';
import { AngularMaterialModule } from '../../../angular-material/angular-material.module';
import { MatDialogRef } from '@angular/material';
import { APP_BASE_HREF } from '@angular/common';

describe('DeleteTeacherDialogComponent', () => {
  let component: DeleteTeacherDialogComponent;
  let fixture: ComponentFixture<DeleteTeacherDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        AngularMaterialModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: APP_BASE_HREF, useValue: '/' },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTeacherDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
