import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { Building } from '../../../models/building';
import { Room, RoomTypeEnum } from '../../../models/room';
import { Teacher } from '../../../models/teacher';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-add-teacher-dialog',
  templateUrl: './add-teacher-dialog.component.html',
  styleUrls: ['./add-teacher-dialog.component.scss']
})
export class AddTeacherDialogComponent implements OnInit {
  newTeacherForm = new FormGroup({
    firstName: new FormControl(this.data.teacher ? this.data.teacher.firstName : '',
      [Validators.required]),
    lastName: new FormControl(this.data.teacher ? this.data.teacher.lastName : '',
      [Validators.required]),
    title: new FormControl(this.data.teacher ? this.data.teacher.title : '',
      [Validators.required])
  });

  office: FormGroup;

  rooms: Room[];
  buildings: Building[];
  buildingsSub: Subscription;

  constructor(
    private commonService: CommonService,
    public dialogRef: MatDialogRef<AddTeacherDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      teacher?: Teacher;
      departmentId?: string
    }
  ) {}

  ngOnInit() {
    this.office = new FormGroup({
    building: new FormControl(this.data.teacher ? this.data.teacher.office.building : ''),
    number: new FormControl({
      value: this.data.teacher ? this.data.teacher.office.number : '',
      disabled: this.office && this.office.controls.building && !this.office.controls.building.value
    }),
    type: new FormControl(this.data.teacher ? this.data.teacher.office.type : '')
    });
    
    this.buildingsSub = this.commonService.getBuildings()
      .subscribe((buildings: Building[]) => {
        this.buildings = buildings;
      });
  }

  defineRoomSelectOptions() {
    this.rooms = null;
    if (this.office.controls.building) {
      this.rooms = this.office.controls.building.value.rooms
        .filter(room => room.type === RoomTypeEnum.OFFICE);
    }
  }

  getRoom(roomNumber: number, roomType: RoomTypeEnum) {
    return `${roomNumber} [${roomType}]`;
  }

  onSaveTeacher() {
    this.newTeacherForm.controls.firstName.markAsTouched();
    this.newTeacherForm.controls.lastName.markAsTouched();
    this.newTeacherForm.controls.title.markAsTouched();

    if (this.newTeacherForm.valid) {
      const newTeacher: Teacher = {
        ...this.newTeacherForm.value,
        id: this.data.teacher ? this.data.teacher.id : null,
        office: {
          ...this.office.controls.number.value,
          building: this.office.controls.building.value
        },
        courses: [],
        departmentId: this.data.departmentId
      };
      this.dialogRef.close(newTeacher);
    }
  }

  onCancel() {
    this.dialogRef.close();
  }
}
