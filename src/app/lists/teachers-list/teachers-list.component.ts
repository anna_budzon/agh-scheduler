import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Course } from '../../models/course';
import { Department } from '../../models/department';
import { Room, RoomTypeEnum } from '../../models/room';
import { Teacher } from '../../models/teacher';
import { TeacherService } from '../../services/teacher.service';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { AddTeacherDialogComponent } from './add-teacher-dialog/add-teacher-dialog.component';
import { DeleteTeacherDialogComponent } from './delete-teacher-dialog/delete-teacher-dialog.component';
import { Globals } from '../../../environments/globals';
import { ErrorSnackBarComponent } from '../../pop-ups/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.scss']
})
export class TeachersListComponent implements OnInit, OnDestroy {

  department: Department;
  dataSource = new MatTableDataSource<Teacher>([]);
  teachers: Teacher[];
  addTeacherSub: Subscription;
  getTeachersSub: Subscription;
  updateTeacherSub: Subscription;
  deleteTeacherSub: Subscription;

  constructor(
    private dialog: MatDialog,
    private teacherService: TeacherService,
    private snackBar: MatSnackBar,
    private router: Router,
    private globals: Globals
  ) { }

  ngOnInit() {
    this.getTeachersSub = this.teacherService
      .getTeachers(this.globals.department.id)
      .subscribe((fetchedTeachers: Teacher[]) => {
        this.teachers = fetchedTeachers;
        this.refresh();
      });
  }

  getColumns() {
    return ['title', 'firstName', 'lastName', 'room', 'displaySchedule', 'edit', 'delete'];
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getRoom(office?: Room) {
    if (!office) {
      return 'BRAK';
    }
    return `${office.building.name} s. ${office.number} [${this.getOfficeType(office.type)}]`;
  }

  getOfficeType(type: RoomTypeEnum) {
    let officeType = '';
    switch (type) {
      case RoomTypeEnum.OFFICE:
        officeType = 'Gabinet';
        break;
      case RoomTypeEnum.IT:
        officeType = 'Sala komputerowa';
        break;
      case RoomTypeEnum.LECTURE:
        officeType = 'Sala wykładowa';
        break;
      case RoomTypeEnum.LAB:
        officeType = 'Sala laboratoryjna';
        break;
      case RoomTypeEnum.PE:
        officeType = 'Sala gimnastyczna';
        break;
      default:
        officeType = 'Sala audytoryjna';
        break;
    }
    return officeType;
  }

  getCourses(courses: Course[]) {
    if (!courses || !courses.length) {
      return 'BRAK';
    }

    const coursesNames: string[] = courses.map(course => course.name);
    return `${coursesNames}`;
  }

  addTeacher() {
    const dialogRef = this.dialog.open(AddTeacherDialogComponent, {
      width: '600px',
      data: { departmentId: this.globals.department.id }
    });

    dialogRef.afterClosed()
      .subscribe((newTeacher: Teacher) => {
        if (newTeacher) {
          this.addTeacherSub = this.teacherService.addTeacher(newTeacher)
            .subscribe((addedTeacher: Teacher) => {
              this.teachers.push({ ...newTeacher, id: addedTeacher.id });
              this.refresh();
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: `Nauczyciel ${addedTeacher.firstName} ${addedTeacher.lastName} został dodany.` },
                  duration: 2000,
                  verticalPosition: 'bottom',
                  horizontalPosition: 'left'
              });
            });
        }
      });
  }

  editTeacher(teacherToUpdate: Teacher) {
    const dialogRef = this.dialog.open(AddTeacherDialogComponent, {
      width: '600px',
      data: {
        teacher: teacherToUpdate,
        departmentId: this.globals.department.id
      }
    });

    dialogRef.afterClosed()
      .subscribe((editedTeacher: Teacher) => {
        if (editedTeacher) {
        this.updateTeacherSub = this.teacherService.updateTeacher(editedTeacher)
          .subscribe((updateTeacher: Teacher) => {
            const idx = this.teachers.findIndex((st: Teacher) =>
              st.id === updateTeacher.id
            );
            this.teachers.splice(idx, 1, updateTeacher);
            this.refresh();
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: `Nauczyciel ${updateTeacher.firstName} ${updateTeacher.lastName} został uaktualniony.` },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
            });
          });
        }
      });
  }

  deleteTeacher(teacherToDelete: Teacher) {
    const dialogRef = this.dialog.open(DeleteTeacherDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.deleteTeacherSub = this.teacherService.deleteTeacher(teacherToDelete.id)
            .subscribe(() => {
              const idx = this.teachers.findIndex((st: Teacher) =>
                st.id === teacherToDelete.id
              );
              this.teachers.splice(idx, 1);
              this.refresh();
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: `Nauczyciel ${teacherToDelete.firstName} ${teacherToDelete.lastName} został usunięty.` },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
            });
        }
      });
  }

  displaySchedule(teacher: Teacher) {
    if (teacher.schedule) {
      this.router.navigate([`schedules/${teacher.schedule.id}`]);
    } else {
      this.snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: { message: 'Prowadzący nie ma jeszcze przygotowanego planu zajęć!' },
        duration: 2000,
        verticalPosition: 'bottom',
        horizontalPosition: 'left'
      });
    }
  }

  goBack() {
    this.router.navigate(['']);
  }

  refresh() {
    this.dataSource.data = this.teachers;
  }

  ngOnDestroy() {
    if (this.addTeacherSub) { this.addTeacherSub.unsubscribe(); }
    if (this.getTeachersSub) { this.getTeachersSub.unsubscribe(); }
    if (this.updateTeacherSub) { this.updateTeacherSub.unsubscribe(); }
    if (this.deleteTeacherSub) { this.deleteTeacherSub.unsubscribe(); }
  }
}

