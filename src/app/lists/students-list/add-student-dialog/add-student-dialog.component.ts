import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { Major } from '../../../models/major';
import { Student } from '../../../models/student';
import { MajorService } from '../../../services/major.service';

@Component({
  selector: 'app-add-student-dialog',
  templateUrl: './add-student-dialog.component.html',
  styleUrls: ['./add-student-dialog.component.scss']
})
export class AddStudentDialogComponent {
  newStudentForm = new FormGroup({
    index: new FormControl(this.data.student ? this.data.student.index : '', [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
      Validators.minLength(6)
    ]),
    firstName: new FormControl(this.data.student ? this.data.student.firstName : '', [
      Validators.required
    ]),
    lastName: new FormControl(this.data.student ? this.data.student.lastName : '', [
      Validators.required
    ]),
    pesel: new FormControl(this.data.student ? this.data.student.pesel : '', [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
      Validators.minLength(11)
    ]),
  });
  majors: Major[];
  getMajorsSub: Subscription;

  constructor(
    private majorService: MajorService,
    public dialogRef: MatDialogRef<AddStudentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      student: Student,
      departmentId: string,
      groupMajorId: string,
      majorId: string
    }
  ) {}

  onSaveStudent() {
    this.newStudentForm.controls.index.markAsTouched();
    this.newStudentForm.controls.firstName.markAsTouched();
    this.newStudentForm.controls.lastName.markAsTouched();
    this.newStudentForm.controls.pesel.markAsTouched();

    if (this.newStudentForm.valid) {

      if (this.data.student) {
        this.dialogRef.close({
          ...this.data.student,
          index: this.newStudentForm.value.index,
          firstName: this.newStudentForm.value.firstName,
          lastName: this.newStudentForm.value.lastName,
          pesel: this.newStudentForm.value.pesel
        });
      } else {
        this.dialogRef.close({
          ...this.newStudentForm.value,
          groupMajorId: this.data.groupMajorId,
          id: this.data.student ? this.data.student.id : null,
          departmentId: this.data.departmentId,
          major: this.data.majorId
        });
      }
    }
  }

  onCancel() {
    this.dialogRef.close();
  }
}
