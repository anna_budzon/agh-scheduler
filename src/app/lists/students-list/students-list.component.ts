import { Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';
import { Major } from '../../models/major';
import { Student } from '../../models/student';
import { StudentService } from '../../services/student.service';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { AddStudentDialogComponent } from './add-student-dialog/add-student-dialog.component';
import { DeleteStudentDialogComponent } from './delete-student-dialog/delete-student-dialog.component';
import { Globals } from '../../../environments/globals';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})
export class StudentsListComponent implements OnChanges, OnDestroy {
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @Input() majorGroupId: string;
  @Input() majorId: Major;

  displayedColumns: string[] = ['index', 'firstName', 'lastName', 'pesel', 'major', 'gr', 'grLab', 'edit', 'delete'];
  dataSource = new MatTableDataSource<Student>([]);
  students: Student[];
  addStudentSub: Subscription;
  getStudentsSub: Subscription;
  updateStudentSub: Subscription;
  deleteStudentSub: Subscription;

  constructor(
    private dialog: MatDialog,
    private studentService: StudentService,
    private snackBar: MatSnackBar,
    private globals: Globals
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['majorGroupId'] && this.majorGroupId) {
      this.getStudentsSub = this.studentService.getStudents(this.majorGroupId)
      .subscribe((fetchedStudents: Student[]) => {
        this.students = fetchedStudents;
        this.refresh();
      });
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  enableSorting() {
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  }

  addStudent() {
    const dialogRef = this.dialog.open(AddStudentDialogComponent, {
      width: '600px',
      data: {
        departmentId: this.globals.department.id,
        majorId: this.majorId,
        groupMajorId: this.majorGroupId
      }
    });

    dialogRef.afterClosed()
      .subscribe((newStudent: Student) => {
        if (newStudent) {
          this.addStudentSub = this.studentService.addStudent(newStudent)
            .subscribe((addedStudent: Student) => {
              this.students.push(addedStudent);
              this.refresh();
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: `Student ${addedStudent.firstName} ${addedStudent.lastName} został dodany.` },
                  duration: 2000,
                  verticalPosition: 'bottom',
                  horizontalPosition: 'left'
              });
            });
        }
      });
  }

  editStudent(studentToUpdate: Student) {
    const dialogRef = this.dialog.open(AddStudentDialogComponent, {
      width: '600px',
      data: { student: studentToUpdate }
    });

    dialogRef.afterClosed()
      .subscribe((editedStudent: Student) => {
        if (editedStudent) {
        this.updateStudentSub = this.studentService.updateStudent(editedStudent)
          .subscribe((updatedStudent: Student) => {
            const idx = this.students.findIndex((st: Student) =>
              st.id === updatedStudent.id
            );
            this.students.splice(idx, 1, editedStudent);
            this.refresh();
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: `Student ${updatedStudent.firstName} ${updatedStudent.lastName} został uaktualniony.` },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
            });
          });
        }
      });
  }

  deleteStudent(studentToDelete: Student) {
    const dialogRef = this.dialog.open(DeleteStudentDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed()
      .subscribe((result) => {
        if (result) {
          this.deleteStudentSub = this.studentService.deleteStudent(studentToDelete.id)
            .subscribe(() => {
              const idx = this.students.findIndex((st: Student) =>
                st.id === studentToDelete.id
              );
              this.students.splice(idx, 1);
              this.refresh();
              this.snackBar.openFromComponent(InformSnackBarComponent, {
                data: { message: `Student ${studentToDelete.firstName} ${studentToDelete.lastName} został usunięty.` },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
            });
        }
      });
  }

  refresh() {
    this.dataSource.data = this.students;
  }

  ngOnDestroy() {
    if (this.addStudentSub) { this.addStudentSub.unsubscribe(); }
    if (this.getStudentsSub) { this.getStudentsSub.unsubscribe(); }
    if (this.updateStudentSub) { this.updateStudentSub.unsubscribe(); }
    if (this.deleteStudentSub) { this.deleteStudentSub.unsubscribe(); }
  }
}
