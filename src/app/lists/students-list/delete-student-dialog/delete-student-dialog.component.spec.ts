import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteStudentDialogComponent } from './delete-student-dialog.component';
import { AppModule } from '../../../app.module';
import { AngularMaterialModule } from '../../../angular-material/angular-material.module';
import { MatDialogRef } from '@angular/material';
import { APP_BASE_HREF } from '@angular/common';

describe('DeleteStudentDialogComponent', () => {
  let component: DeleteStudentDialogComponent;
  let fixture: ComponentFixture<DeleteStudentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        AngularMaterialModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: APP_BASE_HREF, useValue: '/' },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteStudentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
