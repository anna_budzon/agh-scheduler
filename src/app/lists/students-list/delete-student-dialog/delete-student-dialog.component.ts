import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DeleteMajorDialogComponent } from '../../../lists/majors-list/delete-major-dialog/delete-major-dialog.component';

@Component({
  selector: 'app-delete-student-dialog',
  templateUrl: './delete-student-dialog.component.html',
  styleUrls: ['./delete-student-dialog.component.scss']
})
export class DeleteStudentDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteMajorDialogComponent>) {}

  onConfirm() {
    this.dialogRef.close(true);
  }

  onCancel() {
    this.dialogRef.close(false);
  }
}
