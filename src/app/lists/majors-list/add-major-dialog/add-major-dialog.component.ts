import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-major-dialog',
  templateUrl: './add-major-dialog.component.html',
  styleUrls: ['./add-major-dialog.component.scss']
})
export class AddMajorDialogComponent {
  newMajorForm = new FormGroup({
    name: new FormControl('')
  });

  constructor(public dialogRef: MatDialogRef<AddMajorDialogComponent>) {}

  onSaveMajor() {
    this.dialogRef.close(this.newMajorForm.value);
  }

  onCancel() {
    this.dialogRef.close();
  }
}
