import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from '../../../environments/globals';
import { Major, MajorModeEnum, MajorTypeEnum } from '../../models/major';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { MajorService } from '../../services/major.service';
import { AddMajorDialogComponent } from './add-major-dialog/add-major-dialog.component';
import { DeleteMajorDialogComponent } from './delete-major-dialog/delete-major-dialog.component';

@Component({
  selector: 'app-majors-list',
  templateUrl: './majors-list.component.html',
  styleUrls: ['./majors-list.component.scss']
})
export class MajorsListComponent implements OnInit, OnDestroy {
  majors: Major[] = [];
  dataSource = new MatTableDataSource<Major>();
  getMajorsSub: Subscription;
  addMajorSub: Subscription;
  deleteMajorSub: Subscription;

  constructor(
    private majorService: MajorService,
    public dialog: MatDialog,
    public router: Router,
    private snackBar: MatSnackBar,
    public globals: Globals
  ) {}

  ngOnInit() {
    this.getMajorsSub = this.majorService.getMajors(this.globals.department.id)
      .subscribe((majors: Major[]) => {
        this.majors = majors;
        this.refresh();
      });
  }

  checkRoute() {
    return this.router.url === '/majors';
  }

  addMajor() {
    const dialogRef = this.dialog.open(AddMajorDialogComponent, {
      width: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addMajorSub = this.majorService.addMajor(result.name, this.globals.department.id)
          .subscribe((newMajor: Major) => {
            this.majors.push(newMajor);
            this.refresh();
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: `Kierunek '${newMajor.name}' został dodany.` },
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'left'
            });
          });
      }
    });
  }

  seeMoreMajor(majorId: string) {
    this.router.navigate([`majors/${majorId}`]);
  }

  getMajorTypes(major: Major) {
    const first = major.majorType.includes(MajorTypeEnum.FIRST) ? 'I stopień' : '';
    const second = major.majorType.includes(MajorTypeEnum.SECOND) ? 'II stopień' : '';
    let comma = '';
    if (first && second) {
      comma = ', ';
    }

    return `${first}${comma}${second}`;
  }

  getMajorModes(major: Major) {
    const full = major.majorMode.includes(MajorModeEnum.FULL_TIME) ? 'Stacjonarne' : '';
    const part = major.majorMode.includes(MajorModeEnum.PART_TIME) ? 'Niestacjonarne' : '';
    let comma = '';
    if (full && part) {
      comma = ', ';
    }

    return `${full}${comma}${part}`;
  }

  deleteMajor(majorToDelete: Major) {
    const dialogRef = this.dialog.open(DeleteMajorDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteMajorSub = this.majorService.deleteMajor(majorToDelete.id)
          .subscribe(() => {
            const index = this.majors.findIndex(major =>
              major.id === majorToDelete.id
            );
            this.majors.splice(index, 1);
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: `Kierunek ${majorToDelete.name}' został usunięty.` },
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'left'
            });
            this.refresh();
          });
      }
    });
  }

  goBack() {
    this.router.navigate(['']);
  }

  refresh() {
    this.dataSource.data = this.majors;
  }

  ngOnDestroy() {
    if (this.addMajorSub) { this.addMajorSub.unsubscribe(); }
    if (this.getMajorsSub) { this.getMajorsSub.unsubscribe(); }
    if (this.deleteMajorSub) { this.deleteMajorSub.unsubscribe(); }
  }
}
