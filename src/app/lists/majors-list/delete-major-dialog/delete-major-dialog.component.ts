import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-delete-major-dialog',
  templateUrl: './delete-major-dialog.component.html',
  styleUrls: ['./delete-major-dialog.component.scss']
})
export class DeleteMajorDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteMajorDialogComponent>) {}

  onConfirm() {
    this.dialogRef.close(true);
  }

  onCancel() {
    this.dialogRef.close(false);
  }
}
