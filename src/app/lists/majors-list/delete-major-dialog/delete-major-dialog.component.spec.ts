import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteMajorDialogComponent } from './delete-major-dialog.component';
import { AppModule } from '../../../app.module';
import { AngularMaterialModule } from '../../../angular-material/angular-material.module';
import { MatDialogRef } from '@angular/material';
import { APP_BASE_HREF } from '@angular/common';

describe('DeleteMajorDialogComponent', () => {
  let component: DeleteMajorDialogComponent;
  let fixture: ComponentFixture<DeleteMajorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        AngularMaterialModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: APP_BASE_HREF, useValue: '/' },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteMajorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
