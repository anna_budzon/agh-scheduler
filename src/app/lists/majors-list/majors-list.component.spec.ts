import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularMaterialModule } from '../../angular-material/angular-material.module';
import { AppModule } from '../../app.module';
import { MajorsListComponent } from './majors-list.component';
import { APP_BASE_HREF } from '@angular/common';


describe('MajorsListComponent', () => {
  let component: MajorsListComponent;
  let fixture: ComponentFixture<MajorsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        AngularMaterialModule,
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajorsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
