import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajorGroupsListComponent } from './major-groups-list.component';
import { AppModule } from '../../app.module';
import { AngularMaterialModule } from '../../angular-material/angular-material.module';
import { APP_BASE_HREF } from '@angular/common';

describe('MajorGroupsListComponent', () => {
  let component: MajorGroupsListComponent;
  let fixture: ComponentFixture<MajorGroupsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        AngularMaterialModule,
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajorGroupsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
