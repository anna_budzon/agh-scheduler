import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MajorGroup } from '../../models/major-group';
import { MajorService } from '../../services/major.service';
import { InformSnackBarComponent } from '../../pop-ups/inform-snack-bar/inform-snack-bar.component';
import { DeleteMajorDialogComponent } from '../majors-list/delete-major-dialog/delete-major-dialog.component';
import { AddMajorGroupDialogComponent } from './add-major-group-dialog/add-major-group-dialog.component';
import { ErrorSnackBarComponent } from '../../pop-ups/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-major-groups-list',
  templateUrl: './major-groups-list.component.html',
  styleUrls: ['./major-groups-list.component.scss']
})
export class MajorGroupsListComponent implements OnInit, OnDestroy {
  @Input() viewMode = true;
  @Input() majorName: string;

  groups: MajorGroup[];
  majorId: string;
  dataSource = new MatTableDataSource<MajorGroup>();
  addGroupSub: Subscription;
  routeSub: Subscription;
  getGroupSub: Subscription;

  constructor(
    private majorService: MajorService,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.routeSub = this.route.params.subscribe((params) => {
      this.majorId = params['id'];
    });

    this.getGroupSub = this.majorService.getMajorGroups(this.majorId)
      .subscribe((groups: MajorGroup[]) => {
        this.groups = groups;
        this.refresh();
      });
  }

  getColumns() {
    return this.viewMode
      ? ['year', 'semester', 'type', 'mode', 'beginYear', 'displaySchedule']
      : ['year', 'semester', 'type', 'mode', 'beginYear', 'displaySchedule', 'edit', 'delete'];
  }

  refresh() {
    this.dataSource.data = this.groups;
  }

  displayGroupStudents(group: MajorGroup) {
    this.router.navigate([`majors/${this.majorId}/groups/${group.id}`]);
  }

  displaySchedule(group: MajorGroup) {
    if (group.schedule) {
      this.router.navigate([`schedules/${group.schedule}`]);
    } else {
      this.snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: { message: 'Grupa nie ma jeszcze przygotowanego planu zajęć!' },
        duration: 2000,
        verticalPosition: 'bottom',
        horizontalPosition: 'left'
      });
    }
  }

  addGroup() {
    const dialogRef = this.dialog.open(AddMajorGroupDialogComponent, {
      data: { majorId: this.majorId, majorName: this.majorName ? this.majorName : 'brak' },
      width: '600px'
    });

    dialogRef.afterClosed()
      .subscribe((result: MajorGroup) => {
        if (result) {
          this.addGroupSub = this.majorService.addMajorGroup(
            this.majorId,
            result
          ).subscribe((newGroup: MajorGroup) => {
            this.groups.push(newGroup);
            this.refresh();
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: 'Grupa została dodana.' },
              duration: 2000,
              verticalPosition: 'bottom',
              horizontalPosition: 'left'
            });
          });
        }
      });
  }

  editGroup(groupId: string) {
    this.router.navigate([`majors/${this.majorId}/groups/${groupId}`]);
  }

  deleteGroup(groupId: string) {
    const dialogRef = this.dialog.open(DeleteMajorDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.majorService.deleteMajorGroup(this.majorId, groupId)
          .subscribe(() => {
            const idx = this.groups.findIndex(group => group.id === groupId);
            this.groups.splice(idx, 1);
            this.refresh();
            this.snackBar.openFromComponent(InformSnackBarComponent, {
              data: { message: 'Grupa została usunięta' },
                duration: 2000,
                verticalPosition: 'bottom',
                horizontalPosition: 'left'
              });
          });
      }
    });
  }

  ngOnDestroy() {
    if (this.routeSub) { this.routeSub.unsubscribe(); }
    if (this.addGroupSub) { this.addGroupSub.unsubscribe(); }
    if (this.getGroupSub) { this.getGroupSub.unsubscribe(); }
  }
}
