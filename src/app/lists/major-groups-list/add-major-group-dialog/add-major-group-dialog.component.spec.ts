import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMajorGroupDialogComponent } from './add-major-group-dialog.component';
import { AppModule } from '../../../app.module';
import { AngularMaterialModule } from '../../../angular-material/angular-material.module';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { APP_BASE_HREF } from '@angular/common';

describe('AddMajorGroupDialogComponent', () => {
  let component: AddMajorGroupDialogComponent;
  let fixture: ComponentFixture<AddMajorGroupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        AngularMaterialModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] },
        { provide: APP_BASE_HREF, useValue: '/' },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMajorGroupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
