import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddStudentDialogComponent } from '../../../lists/students-list/add-student-dialog/add-student-dialog.component';
import { MajorModeEnum, MajorTypeEnum } from '../../../models/major';
import { isYearValidator } from '../../../validators/isYear.validator';

@Component({
  selector: 'app-add-major-group-dialog',
  templateUrl: './add-major-group-dialog.component.html',
  styleUrls: ['./add-major-group-dialog.component.scss']
})
export class AddMajorGroupDialogComponent implements OnInit {
  types = [ MajorTypeEnum.FIRST, MajorTypeEnum.SECOND ];
  modes = [ MajorModeEnum.FULL_TIME, MajorModeEnum.PART_TIME ];
  years = [];
  semesters =  [];
  newGroupForm = new FormGroup({
    type: new FormControl('', [
      Validators.required
    ]),
    mode: new FormControl('',  [
      Validators.required
    ]),
    year: new FormControl('',  [
      Validators.required
    ]),
    semester: new FormControl('',  [
      Validators.required
    ]),
    beginYear: new FormControl('',  [
      Validators.required,
      isYearValidator(new Date().getFullYear())
    ])
  });

  constructor(
    public dialogRef: MatDialogRef<AddStudentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { majorId: string, majorName: string }
  ) {}

  ngOnInit() {
    this.defineSelectOptions();
  }

  onSaveStudent() {
    this.newGroupForm.controls.type.markAsTouched();
    this.newGroupForm.controls.mode.markAsTouched();
    this.newGroupForm.controls.year.markAsTouched();
    this.newGroupForm.controls.semester.markAsTouched();
    this.newGroupForm.controls.beginYear.markAsTouched();

    if (this.newGroupForm.valid) {
      this.dialogRef.close({
        parentMajor: this.data.majorId,
       ...this.newGroupForm.value
      });
    }
  }

  defineSelectOptions() {
    if (this.newGroupForm.controls.mode.value === MajorModeEnum.FULL_TIME) {
      this.years = [1, 2, 3, 4, 5];
      this.semesters = this.newGroupForm.controls.type.value === MajorTypeEnum.FIRST ?
        [1, 2, 3, 4, 5, 6, 7] :
        [1, 2, 3];
    } else {
      this.years = [1, 2, 3, 4, 5, 6];
      this.semesters = this.newGroupForm.controls.type.value === MajorTypeEnum.FIRST ?
        [1, 2, 3, 4, 5, 6, 7, 8] :
        [1, 2, 3, 4];
    }
  }

  onCancel() {
    this.dialogRef.close();
  }
}
