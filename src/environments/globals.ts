import { Injectable } from '@angular/core';
import { Department } from '../app/models/department';

@Injectable()
export class Globals {
  department: Department = {
    id: '5c6431f49132f3266097476d',
    name: 'Wydział Inżynierii Metali i Informatyki Przemysłowej'
  };
}
